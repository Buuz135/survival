package com.buuz135.survival.hologram;

import com.buuz135.survival.flag.Flag;
import com.buuz135.survival.protectedarea.SurvivalPlot;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class HologramHandler implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.hasItem() && event.getItem().getType() == Material.ARMOR_STAND && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName()) {
            Location location = event.getClickedBlock().getLocation().clone();
            location.add(0.5, 0.5, 0.5);
            SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(location);
            if (plot != null && plot.isMember(event.getPlayer().getUniqueId()) && plot.getFlaresMembers().contains(Flag.PLACE)) {
                new Hologram(event.getPlayer().getUniqueId(), ChatColor.translateAlternateColorCodes('&', event.getItem().getItemMeta().getDisplayName()), location);
                event.setCancelled(true);
            }
        }
    }

}
