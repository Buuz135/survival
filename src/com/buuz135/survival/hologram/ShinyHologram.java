package com.buuz135.survival.hologram;

import com.buuz135.survival.SurvivalMain;
import net.minecraft.server.v1_9_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;

public class ShinyHologram {

    public static List<ShinyHologram> shinyHolograms = new ArrayList<>();

    static {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(SurvivalMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    for (ShinyHologram s : shinyHolograms) {
                        s.sendToPlayer(player, false);
                    }
                }
            }
        }, 0, 4 * 60 * 20);
        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onJoin(PlayerJoinEvent event) {
                shinyHolograms.stream().forEach(shinyHologram -> shinyHologram.sendToPlayer(event.getPlayer(), true));
            }
        }, SurvivalMain.getInstance());
    }

    private List<String> lines;
    private Location location;
    private List<EntityExperienceOrb> entityExperienceOrbs;
    private List<EntityArmorStand> entityArmorStands;

    public ShinyHologram(List<String> lines, Location location, int orbLineOffset) {
        this.lines = lines;
        this.location = location;
        this.entityExperienceOrbs = new ArrayList<>();
        this.entityArmorStands = new ArrayList<>();
        Location top1 = new Location(location.getWorld(), location.getX(), location.getY() + orbLineOffset + 0.5 + (lines.size() / 2) * 0.25, location.getZ());
        createOrb(top1, 1);
        Location top2 = new Location(location.getWorld(), location.getX(), location.getY() + orbLineOffset + 0.25 + (lines.size() / 2) * 0.25, location.getZ());
        createOrb(top2, 100);
        Location top3 = new Location(location.getWorld(), location.getX(), location.getY() - orbLineOffset - 0.25 - ((lines.size() - 1) / 2) * 0.25, location.getZ());
        createOrb(top3, 100);
        Location top4 = new Location(location.getWorld(), location.getX(), location.getY() - orbLineOffset - 0.5 - ((lines.size() - 1) / 2) * 0.25, location.getZ());
        createOrb(top4, 1);
        shinyHolograms.add(this);
    }

    public void sendToPlayer(Player player, boolean stand) {
        constructOrb(player, entityExperienceOrbs.get(0), entityArmorStands.get(0));
        constructOrb(player, entityExperienceOrbs.get(1), entityArmorStands.get(1));
        if (stand) constructStand(player);
        constructOrb(player, entityExperienceOrbs.get(2), entityArmorStands.get(2));
        constructOrb(player, entityExperienceOrbs.get(3), entityArmorStands.get(3));
    }

    private void constructOrb(Player player, EntityExperienceOrb orb, EntityArmorStand armorStand) {
        armorStand.setInvisible(true);
        orb.startRiding(armorStand);
        CraftPlayer craftplayer = (CraftPlayer) player;
        craftplayer.getHandle().playerConnection.sendPacket(new PacketPlayOutSpawnEntityExperienceOrb(orb));
        craftplayer.getHandle().playerConnection.sendPacket(new PacketPlayOutSpawnEntity(armorStand, 78));
        craftplayer.getHandle().playerConnection.sendPacket(new PacketPlayOutMount(armorStand));
        craftplayer.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(armorStand.getId(), armorStand.getDataWatcher(), true));
        craftplayer.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(orb.getId(), orb.getDataWatcher(), true));

    }

    private void createOrb(Location location, int value) {
        CraftWorld world = (CraftWorld) location.getWorld();
        EntityExperienceOrb orb = new EntityExperienceOrb(world.getHandle(), location.getX(), location.getY() + 1.48, location.getZ(), value);
        EntityArmorStand armorStand = new EntityArmorStand(world.getHandle(), location.getX(), location.getY(), location.getZ());
        entityExperienceOrbs.add(orb);
        entityArmorStands.add(armorStand);
    }

    private void constructStand(Player player) {
        CraftWorld world = (CraftWorld) location.getWorld();
        int i = 0;
        for (String s : lines) {
            double modifier = 0;
            if (i < lines.size() / 2) modifier = ((lines.size() / 2) - i) * 0.25;
            if (i > lines.size() / 2) modifier = -(i - lines.size() / 2) * 0.25;
            EntityArmorStand armorStand = new EntityArmorStand(world.getHandle(), location.getX(), location.getY() - 0.75 + modifier, location.getZ());
            armorStand.setInvisible(true);
            armorStand.setCustomName(s);
            armorStand.setCustomNameVisible(true);
            CraftPlayer craftplayer = (CraftPlayer) player;
            craftplayer.getHandle().playerConnection.sendPacket(new PacketPlayOutSpawnEntity(armorStand, 78));
            craftplayer.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(armorStand.getId(), armorStand.getDataWatcher(), true));
            ++i;
        }
    }


    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<EntityExperienceOrb> getEntityExperienceOrbs() {
        return entityExperienceOrbs;
    }

}
