package com.buuz135.survival.hologram;

import com.buuz135.survival.utils.SerializationUtils;
import com.google.gson.JsonObject;
import net.minecraft.server.v1_9_R2.EntityArmorStand;
import net.minecraft.server.v1_9_R2.World;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R2.CraftWorld;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Hologram {

    public static List<Hologram> hologramList = new ArrayList<>();

    private Location location;
    private UUID owner;
    private String display;
    private EntityArmorStand armorStand;
    private List<Player> playersCanSee;

    public Hologram(UUID owner, String display, Location location) {
        this.owner = owner;
        this.display = display;
        this.location = location;
        this.playersCanSee = new ArrayList<>();
        World world = ((CraftWorld) location.getWorld()).getHandle();
        this.armorStand = new EntityArmorStand(world, location.getX(), location.getY(), location.getZ());
        this.armorStand.setInvisible(true);
        this.armorStand.setGravity(false);
        this.armorStand.setCustomName(display);
        this.armorStand.setCustomNameVisible(true);
        hologramList.add(this);
    }

    public Hologram(JsonObject jsonObject) {
        this(UUID.fromString(jsonObject.get("owner").getAsString()), jsonObject.get("display").getAsString(), SerializationUtils.deserializeLocation(jsonObject.get("location").getAsJsonObject()));
    }

    public List<Player> getPlayersCanSee() {
        return playersCanSee;
    }

    public EntityArmorStand getArmorStand() {
        return armorStand;
    }

    public Location getLocation() {
        return location;
    }

    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("owner", owner.toString());
        jsonObject.add("location", SerializationUtils.serializeLocation(location));
        jsonObject.addProperty("display", display);
        return jsonObject;
    }

}
