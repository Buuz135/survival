package com.buuz135.survival.jobs;

import com.google.gson.JsonObject;

import java.util.Date;
import java.util.UUID;

public class WorkingPlayer {

    private double energy;
    private Date contracted;
    private UUID player;
    private double totalEnergy;

    public WorkingPlayer(UUID player, double energy, Date contracted, double totalEnergy) {
        this.energy = energy;
        this.contracted = contracted;
        this.player = player;
        this.totalEnergy = totalEnergy;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public Date getContracted() {
        return contracted;
    }

    public void setContracted(Date contracted) {
        this.contracted = contracted;
    }

    public UUID getPlayer() {
        return player;
    }

    public void setPlayer(UUID player) {
        this.player = player;
    }

    public double getTotalEnergy() {
        return totalEnergy;
    }

    public void setTotalEnergy(double totalEnergy) {
        this.totalEnergy = totalEnergy;
    }

    public void addEnergy(double energy) {
        this.energy += energy;
        this.totalEnergy += energy;
    }

    public JsonObject getJsonObject(){
        JsonObject object = new JsonObject();
        object.addProperty("energy",energy);
        object.addProperty("contracted",contracted.getTime());
        object.addProperty("uuid",player.toString());
        object.addProperty("totalEnergy",totalEnergy);
        return object;
    }
}
