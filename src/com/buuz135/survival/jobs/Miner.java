package com.buuz135.survival.jobs;

import com.buuz135.survival.SurvivalMain;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Miner implements IBasicJob {

    public static List<Material> materials = Arrays.asList(Material.COAL_ORE, Material.IRON_ORE, Material.GOLD_ORE, Material.DIAMOND_ORE, Material.EMERALD_ORE, Material.LAPIS_ORE, Material.QUARTZ_ORE, Material.REDSTONE_ORE, Material.GLOWING_REDSTONE_ORE);

    private String name;
    private String id;
    private double maxEnergy;
    private double energyOnUse;
    private List<WorkingPlayer> workingPlayers;
    private int maxWorkers;
    private int maxDays;
    private String description;
    private double totalEnergy;
    private int pay;

    public Miner() {
        this.name = "Minero";
        this.id = "miner";
        this.maxEnergy = 10;
        this.energyOnUse = 0.08;
        this.workingPlayers = new ArrayList<>();
        this.maxWorkers = 10;
        this.maxDays = 7;
        this.description = "-Temporal description-";
        this.totalEnergy = 0.75 * maxDays * maxEnergy;
        this.pay = 10000;
        SurvivalMain.getInstance().getChunkManager().getMaterials().addAll(materials);
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof BlockBreakEvent) {
            BlockBreakEvent breakEvent = (BlockBreakEvent) event;
            System.out.println(breakEvent.getBlock().getType());
            if (materials.contains(breakEvent.getBlock().getType())) {
                if (!SurvivalMain.getInstance().getChunkManager().isPlayerPlaced(breakEvent.getBlock())) {
                    WorkingPlayer player = SurvivalMain.getInstance().getJobsManager().getWorkingPlayerFromJob(((BlockBreakEvent) event).getPlayer(), this);
                    if (player != null && player.getEnergy() < maxEnergy && player.getTotalEnergy() < totalEnergy)
                        player.addEnergy(energyOnUse + breakEvent.getExpToDrop() / 100D);
                }
            }
        }
    }

    @Override
    public double getEnergyConsumed() {
        return energyOnUse;
    }

    @Override
    public double getMaxEnergy() {
        return maxEnergy;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public List<WorkingPlayer> getWorkingPlayers() {
        return workingPlayers;
    }

    @Override
    public int getMaxWorkers() {
        return maxWorkers;
    }

    @Override
    public int getWorkingDays() {
        return maxDays;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getTotalEnergy() {
        return totalEnergy;
    }

    @Override
    public int getPay() {
        return pay;
    }

}
