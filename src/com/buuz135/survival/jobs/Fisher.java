package com.buuz135.survival.jobs;

import com.buuz135.survival.SurvivalMain;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerFishEvent;

import java.util.ArrayList;
import java.util.List;

public class Fisher implements IBasicJob {

    private String name;
    private String id;
    private double maxEnergy;
    private double energyOnUse;
    private List<WorkingPlayer> workingPlayers;
    private int maxWorkers;
    private int maxDays;
    private String description;
    private double totalEnergy;
    private int pay;

    public Fisher() {
        this.name = "Pescador";
        this.id = "fisher";
        this.maxEnergy = 10;
        this.energyOnUse = 0.1;
        this.workingPlayers = new ArrayList<>();
        this.maxWorkers = 10;
        this.maxDays = 7;
        this.description = "-Temporal description-";
        this.totalEnergy = 0.75 * maxDays * maxEnergy;
        this.pay = 10000;
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof PlayerFishEvent) {
            PlayerFishEvent fishEvent = (PlayerFishEvent) event;
            if (fishEvent.getState() == PlayerFishEvent.State.CAUGHT_FISH) {
                WorkingPlayer player = SurvivalMain.getInstance().getJobsManager().getWorkingPlayerFromJob(fishEvent.getPlayer(), this);
                if (player != null && player.getEnergy() < maxEnergy && player.getTotalEnergy() < totalEnergy)
                    player.addEnergy(fishEvent.getExpToDrop() / 30D);
            }
        }
    }

    @Override
    public double getEnergyConsumed() {
        return energyOnUse;
    }

    @Override
    public double getMaxEnergy() {
        return maxEnergy;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public List<WorkingPlayer> getWorkingPlayers() {
        return workingPlayers;
    }


    @Override
    public int getMaxWorkers() {
        return maxWorkers;
    }

    @Override
    public int getWorkingDays() {
        return maxDays;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getTotalEnergy() {
        return totalEnergy;
    }

    @Override
    public int getPay() {
        return pay;
    }

}
