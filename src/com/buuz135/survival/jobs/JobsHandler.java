package com.buuz135.survival.jobs;

import com.buuz135.survival.SurvivalMain;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Date;

public class JobsHandler implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked() instanceof Villager) {
            event.getPlayer().openInventory(SurvivalMain.getInstance().getJobsManager().getJobsInventory());
        }
    }

    @EventHandler
    public void onInvOpen(InventoryOpenEvent event) {
        if (event.getInventory().getType() == InventoryType.MERCHANT) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getClickedInventory().getName().contains("Trabajos")) {
            event.setCancelled(true);
            if (event.getClickedInventory().getItem(event.getSlot()) != null) {
                IBasicJob basicJob = SurvivalMain.getInstance().getJobsManager().getJobs().get(event.getSlot());
                if (basicJob.getMaxWorkers() > basicJob.getWorkingPlayers().size()) {
                    if (SurvivalMain.getInstance().getJobsManager().getPlayerJob((Player) event.getWhoClicked()) == null) {
                        basicJob.getWorkingPlayers().add(new WorkingPlayer(event.getWhoClicked().getUniqueId(), 0, new Date(), 0));
                        event.getWhoClicked().sendMessage(SurvivalMain.TAG + ChatColor.GREEN + "Has sido contratado satisfactoriamente.");
                        event.getWhoClicked().openInventory(SurvivalMain.getInstance().getJobsManager().getJobsInventory());
                    } else {
                        event.getWhoClicked().sendMessage(SurvivalMain.TAG + ChatColor.RED + "Ya tienes un contrato activo.");
                    }
                } else {
                    event.getWhoClicked().sendMessage(SurvivalMain.TAG + ChatColor.RED + "No quedan contratos restantes.");
                }
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        SurvivalMain.getInstance().getJobsManager().fireEvent(event);
    }

    @EventHandler
    public void onFish(PlayerFishEvent event) {
        SurvivalMain.getInstance().getJobsManager().fireEvent(event);
    }

    @EventHandler
    public void onSpawn(CreatureSpawnEvent event) {
        if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SPAWNER)
            event.getEntity().setMetadata("spawner", new FixedMetadataValue(SurvivalMain.getInstance(), true));
    }

    @EventHandler
    public void onInteraact(PlayerInteractEvent event) {
        SurvivalMain.getInstance().getJobsManager().fireEvent(event);
    }

    @EventHandler
    public void onKillEntity(EntityDamageByEntityEvent event) {
        SurvivalMain.getInstance().getJobsManager().fireEvent(event);
    }

    @EventHandler
    public void onEnchant(EnchantItemEvent event) {
        SurvivalMain.getInstance().getJobsManager().fireEvent(event);
    }
}
