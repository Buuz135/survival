package com.buuz135.survival.jobs;

import org.bukkit.event.Event;

import java.util.List;

public interface IBasicJob {

    void onEvent(Event event);

    double getEnergyConsumed();

    double getMaxEnergy();

    String getName();

    String getID();

    List<WorkingPlayer> getWorkingPlayers();

    int getMaxWorkers();

    int getWorkingDays();

    String getDescription();

    double getTotalEnergy();

    int getPay();

}
