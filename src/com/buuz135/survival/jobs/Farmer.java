package com.buuz135.survival.jobs;

import com.buuz135.survival.SurvivalMain;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.List;

public class Farmer implements IBasicJob {
    private String name;
    private String id;
    private double maxEnergy;
    private double energyOnUse;
    private List<WorkingPlayer> workingPlayers;
    private int maxWorkers;
    private int maxDays;
    private String description;
    private double totalEnergy;
    private int pay;

    public Farmer() {
        this.name = "Agricultor";
        this.id = "farmer";
        this.maxEnergy = 10;
        this.energyOnUse = 0.02;
        this.workingPlayers = new ArrayList<>();
        this.maxWorkers = 10;
        this.maxDays = 7;
        this.description = "-Temporal description-";
        this.totalEnergy = 0.75 * maxDays * maxEnergy;
        this.pay = 10000;
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof BlockBreakEvent) {
            if (((BlockBreakEvent) event).getBlock().getType().equals(Material.CROPS) && ((BlockBreakEvent) event).getBlock().getData() == 7) {
                WorkingPlayer player = SurvivalMain.getInstance().getJobsManager().getWorkingPlayerFromJob(((BlockBreakEvent) event).getPlayer(), this);
                if (player != null && player.getEnergy() < maxEnergy && player.getTotalEnergy() < totalEnergy) {
                    player.addEnergy(energyOnUse);
                }
            }
        }
        if (event instanceof PlayerInteractEvent) {
            PlayerInteractEvent interactEvent = (PlayerInteractEvent) event;
            if (interactEvent.getAction() == Action.RIGHT_CLICK_BLOCK && interactEvent.hasItem() && interactEvent.getItem().getType() == Material.INK_SACK && interactEvent.getItem().getDurability() == 15 &&
                    interactEvent.getClickedBlock().getType().equals(Material.CROPS) && interactEvent.getClickedBlock().getData() != 7) {
                WorkingPlayer player = SurvivalMain.getInstance().getJobsManager().getWorkingPlayerFromJob(interactEvent.getPlayer(), this);
                if (player != null && player.getEnergy() < maxEnergy && player.getTotalEnergy() < totalEnergy)
                    player.addEnergy(energyOnUse / 2D);
            }
        }
    }

    @Override
    public double getEnergyConsumed() {
        return energyOnUse;
    }

    @Override
    public double getMaxEnergy() {
        return maxEnergy;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public List<WorkingPlayer> getWorkingPlayers() {
        return workingPlayers;
    }

    @Override
    public int getMaxWorkers() {
        return maxWorkers;
    }

    @Override
    public int getWorkingDays() {
        return maxDays;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getTotalEnergy() {
        return totalEnergy;
    }

    @Override
    public int getPay() {
        return pay;
    }

}
