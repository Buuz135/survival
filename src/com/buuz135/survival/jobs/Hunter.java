package com.buuz135.survival.jobs;

import com.buuz135.survival.SurvivalMain;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hunter implements IBasicJob {

    private static List<EntityType> entityTypes = Arrays.asList(EntityType.OCELOT, EntityType.SHEEP, EntityType.MUSHROOM_COW, EntityType.SQUID, EntityType.CHICKEN, EntityType.COW, EntityType.WOLF, EntityType.BAT, EntityType.PIG, EntityType.HORSE, EntityType.RABBIT, EntityType.VILLAGER);

    private String name;
    private String id;
    private double maxEnergy;
    private double energyOnUse;
    private List<WorkingPlayer> workingPlayers;
    private int maxWorkers;
    private int maxDays;
    private String description;
    private double totalEnergy;
    private int pay;

    public Hunter() {
        this.name = "Cazador";
        this.id = "hunter";
        this.maxEnergy = 10;
        this.energyOnUse = 0.1;
        this.workingPlayers = new ArrayList<>();
        this.maxWorkers = 10;
        this.maxDays = 7;
        this.description = "-Temporal description-";
        this.totalEnergy = 0.75 * maxDays * maxEnergy;
        this.pay = 10000;
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent entityEvent = (EntityDamageByEntityEvent) event;
            if (entityEvent.getDamager() instanceof Player) {
                WorkingPlayer player = SurvivalMain.getInstance().getJobsManager().getWorkingPlayerFromJob((Player) entityEvent.getDamager(), this);
                if (player != null && entityTypes.contains(entityEvent.getEntity().getType()) && entityEvent.getFinalDamage() >= ((LivingEntity) entityEvent.getEntity()).getHealth() && !entityEvent.getEntity().hasMetadata("spawner")) {
                    player.addEnergy(0.1);
                }
            }
        }
    }

    @Override
    public double getEnergyConsumed() {
        return energyOnUse;
    }

    @Override
    public double getMaxEnergy() {
        return maxEnergy;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public List<WorkingPlayer> getWorkingPlayers() {
        return workingPlayers;
    }

    @Override
    public int getMaxWorkers() {
        return maxWorkers;
    }

    @Override
    public int getWorkingDays() {
        return maxDays;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getTotalEnergy() {
        return totalEnergy;
    }

    @Override
    public int getPay() {
        return pay;
    }

}
