package com.buuz135.survival.jobs;

import com.buuz135.survival.SurvivalMain;
import org.bukkit.event.Event;
import org.bukkit.event.enchantment.EnchantItemEvent;

import java.util.ArrayList;
import java.util.List;

public class Enchanter implements IBasicJob {
    private String name;
    private String id;
    private double maxEnergy;
    private double energyOnUse;
    private List<WorkingPlayer> workingPlayers;
    private int maxWorkers;
    private int maxDays;
    private String description;
    private double totalEnergy;
    private int pay;

    public Enchanter() {
        this.name = "Encantador";
        this.id = "enchanter";
        this.maxEnergy = 10;
        this.energyOnUse = 0.5;
        this.workingPlayers = new ArrayList<>();
        this.maxWorkers = 10;
        this.maxDays = 7;
        this.description = "-Temporal description-";
        this.totalEnergy = 0.75 * maxDays * maxEnergy;
        this.pay = 10000;
    }

    @Override
    public void onEvent(Event event) {
        if (event instanceof EnchantItemEvent) {
            EnchantItemEvent enchantItemEvent = (EnchantItemEvent) event;
            WorkingPlayer player = SurvivalMain.getInstance().getJobsManager().getWorkingPlayerFromJob(enchantItemEvent.getEnchanter(), this);
            if (player != null && player.getEnergy() < maxEnergy && player.getTotalEnergy() < totalEnergy)
                player.addEnergy((enchantItemEvent.getExpLevelCost() * enchantItemEvent.getEnchantsToAdd().size()) / 70D);
        }
    }

    @Override
    public double getEnergyConsumed() {
        return energyOnUse;
    }

    @Override
    public double getMaxEnergy() {
        return maxEnergy;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public List<WorkingPlayer> getWorkingPlayers() {
        return workingPlayers;
    }

    @Override
    public int getMaxWorkers() {
        return maxWorkers;
    }

    @Override
    public int getWorkingDays() {
        return maxDays;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getTotalEnergy() {
        return totalEnergy;
    }

    @Override
    public int getPay() {
        return pay;
    }

}
