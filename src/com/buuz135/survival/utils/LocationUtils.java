package com.buuz135.survival.utils;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;

import java.util.Random;

public class LocationUtils {

    public static Location getRandomLocationFromOutside(Location center, int size, int tries) {
        if (tries == 20) {
            return center.getWorld().getSpawnLocation();
        }
        Random random = new Random();
        double coord1 = size / 2 + 1.5;
        double coord2 = random.nextInt(size / 2) + 1.5;
        if (random.nextBoolean()) {
            Location l = getRandomLocation(coord1, coord2, random, center);
            if (l != null) return l;
            return getRandomLocationFromOutside(center, size, ++tries);
        } else {
            Location l = getRandomLocation(coord2, coord1, random, center);
            if (l != null) return l;
            return getRandomLocationFromOutside(center, size, ++tries);
        }

    }

    private static Location getRandomLocation(double coord1, double coord2, Random random, Location center) {
        if (random.nextBoolean()) coord1 = -coord1;
        if (random.nextBoolean()) coord2 = -coord2;
        Location location = new Location(center.getWorld(), center.getX() + coord1, center.getY(), center.getZ() + coord2);
        Location high = location.getWorld().getHighestBlockAt(location).getLocation();
        if (high.getBlock().getRelative(BlockFace.DOWN).getType().isSolid()) {
            return high;
        }
        return null;
    }
}
