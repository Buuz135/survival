package com.buuz135.survival.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.util.UUID;

public class SkullUtils {


    public static GameProfile getNonPlayerProfile(String uuid, String encodedString) {
        GameProfile newSkinProfile = new GameProfile(UUID.fromString(uuid), null);
        newSkinProfile.getProperties().put("textures", new Property("textures", encodedString));
        return newSkinProfile;
    }

    public static ItemStack createItemSkull(GameProfile profile) {
        ItemStack stack = new ItemStack(Material.SKULL_ITEM);
        stack.setDurability((short) 3);
        ItemMeta meta = stack.getItemMeta();
        Field profileField = null;
        try {
            profileField = meta.getClass().getDeclaredField("profile");
        } catch (NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
        profileField.setAccessible(true);
        try {
            profileField.set(meta, profile);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        stack.setItemMeta(meta);
        return stack;
    }
}
