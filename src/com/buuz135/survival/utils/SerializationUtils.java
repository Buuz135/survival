package com.buuz135.survival.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SerializationUtils {

    public static JsonObject serializeItemStack(ItemStack item) {
        JsonObject itemObject = new JsonObject();
        itemObject.addProperty("material", item.getType().name());
        itemObject.addProperty("amount", item.getAmount());
        itemObject.addProperty("durability", item.getDurability());
        itemObject.addProperty("displayname", item.getItemMeta().getDisplayName());
        if (item.getItemMeta().hasLore()) {
            JsonArray lore = new JsonArray();
            for (String l : item.getItemMeta().getLore()) {
                JsonObject loreObject = new JsonObject();
                loreObject.addProperty("line", l);
                lore.add(loreObject);
            }
            itemObject.add("lore", lore);
        }
        if (item.getEnchantments().size() > 0) {
            JsonArray enchantments = new JsonArray();
            for (Enchantment enchantment : item.getItemMeta().getEnchants().keySet()) {
                JsonObject ench = new JsonObject();
                ench.addProperty("enchantment", enchantment.getName());
                ench.addProperty("level", item.getItemMeta().getEnchants().get(enchantment));
                enchantments.add(ench);
            }
            itemObject.add("enchantments", enchantments);
        }
        return itemObject;
    }

    public static ItemStack deserializeItemStack(JsonObject object) {
        Material mat = Material.getMaterial(object.get("material").getAsString());
        int amount = object.get("amount").getAsInt();
        short durability = object.get("durability").getAsShort();
        ItemStack item = new ItemStack(mat, amount, durability);
        ItemMeta meta = item.getItemMeta();

        if (object.get("displayname") != null)
            meta.setDisplayName(object.get("displayname").getAsString());

        if (object.get("lore") != null) {
            List<String> lore = new ArrayList<>();
            for (JsonElement el : object.get("lore").getAsJsonArray()) {
                JsonObject loreElement = el.getAsJsonObject();
                lore.add(loreElement.get("line").getAsString());
            }
            meta.setLore(lore);

        }

        if (object.get("enchantments") != null) {
            for (JsonElement el : object.get("enchantments").getAsJsonArray()) {
                JsonObject enchantment = el.getAsJsonObject();
                meta.addEnchant(Enchantment.getByName(enchantment.get("enchantment").getAsString()), enchantment.get("level").getAsInt(), true);
            }
        }
        item.setItemMeta(meta);
        return item;
    }

    public static JsonObject serializeLocation(Location location) {
        JsonObject l = new JsonObject();
        for (String s : location.serialize().keySet()) {
            if (location.serialize().get(s) instanceof Double || location.serialize().get(s) instanceof Integer) {
                l.addProperty(s, (Number) location.serialize().get(s));
            }
            if (location.serialize().get(s) instanceof String) {
                l.addProperty(s, String.valueOf(location.serialize().get(s)));
            }
        }
        return l;
    }

    public static Location deserializeLocation(JsonObject object) {
        Map<String, Object> ser = new HashMap<>();
        for (Map.Entry<String, JsonElement> s : object.entrySet()) {
            if (s.getValue().getAsJsonPrimitive().isNumber()) {
                try {
                    ser.put(s.getKey(), Integer.valueOf(s.getValue().getAsString()));
                } catch (NumberFormatException e) {
                    ser.put(s.getKey(), Double.valueOf(s.getValue().getAsString()));
                }
            }
            if (s.getValue().getAsJsonPrimitive().isString()) {
                ser.put(s.getKey(), s.getValue().getAsString());
            }
        }
        return Location.deserialize(ser);
    }

    public static String serializeLoc(Location location) {
        return location.getWorld().getName() + "," + location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ();
    }
}
