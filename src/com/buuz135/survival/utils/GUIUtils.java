package com.buuz135.survival.utils;

import net.md_5.bungee.api.ChatColor;

public class GUIUtils {


    public static String hideInfo(String s) {
        String hidden = "";
        for (char c : s.toCharArray()) hidden = hidden + "" + ChatColor.COLOR_CHAR + "" + c;
        return hidden;
    }

    public static String getLoreHideInfo(String entry) {
        return entry.replaceAll("§", "");
    }
}