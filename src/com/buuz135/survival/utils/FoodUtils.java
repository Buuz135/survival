package com.buuz135.survival.utils;

import com.buuz135.survival.foods.FoodPlant;
import com.buuz135.survival.foods.FoodRecipe;
import com.buuz135.survival.foods.FoodTree;
import com.buuz135.survival.foods.IFood;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FoodUtils {

    public static IFood getFoodFromItemStack(ItemStack stack) {
        for (IFood f : FoodPlant.foodPlantList) {
            if (f.getItem().isSimilar(stack)) {
                return f;
            }
        }
        return null;
    }

    public static FoodPlant getFoodPlantFromInt(int i) {
        for (FoodPlant f : FoodPlant.foodPlantList) {
            if (f.getId() == i) {
                return f;
            }
        }
        return null;
    }

    public static IFood getFoodFromUUID(String i) {
        List<IFood> foods = new ArrayList<>();
        foods.addAll(FoodPlant.foodPlantList);
        foods.addAll(FoodRecipe.foodRecipeList);
        foods.addAll(FoodTree.foodTreeList);
        for (IFood f : foods) {
            if (f.getUUID().equals(i)) {
                return f;
            }
        }
        return null;
    }

    public static ItemStack getRandomCropDrop(Random random) {
        return FoodPlant.foodPlantList.get(random.nextInt(FoodPlant.foodPlantList.size())).getItem();
    }

    public static ItemStack getRandomTreeDrop(Random random) {
        return FoodTree.foodTreeList.get(random.nextInt(FoodTree.foodTreeList.size())).getItem();
    }

    public static void decrement(Player player) {
        ItemStack stack = player.getItemInHand();
        if (stack.getAmount() > 1) {
            stack.setAmount(stack.getAmount() - 1);
        } else {
            player.setItemInHand(new ItemStack(Material.AIR));
        }
    }
}
