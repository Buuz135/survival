package com.buuz135.survival.portals;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.utils.AnvilGUI;
import com.buuz135.survival.utils.FoodUtils;
import com.buuz135.survival.utils.ItemStackBuilder;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class PortalHandler implements Listener {


    @EventHandler
    public void onCraft(CraftItemEvent event) {
        if (event.getRecipe().getResult().hasItemMeta() && event.getRecipe().getResult().getItemMeta().getDisplayName().equals(PlayerPortal.name)) {
            ItemStack stack = new ItemStack(Material.PAPER, 2);
            ItemMeta meta = stack.getItemMeta();
            meta.setDisplayName(PlayerPortal.name);
            meta.setLore(Arrays.asList(ChatColor.BLUE + "" + ChatColor.BOLD + "Color: " + ChatColor.DARK_AQUA + new Random().nextInt(15), ChatColor.BLUE + "" + ChatColor.BOLD + "Id: " + ChatColor.DARK_AQUA + UUID.randomUUID()));
            stack.setItemMeta(meta);
            event.setCurrentItem(stack);
            Bukkit.getScheduler().scheduleSyncDelayedTask(SurvivalMain.getInstance(), new Runnable() {
                @Override
                public void run() {
                    event.getInventory().setMatrix(event.getInventory().getMatrix());
                }
            }, 5);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType().name().contains("STEP") && event.hasItem() && event.getItem().hasItemMeta() && event.getItem().getItemMeta().getDisplayName().equals(PlayerPortal.name)) {
            PlayerPortal portal = PlayerPortal.getPlayerPortalByPlayer(event.getPlayer());
            portal.addPoint(getIdFromItem(event.getItem()),event.getClickedBlock().getLocation(), getColorFromItem(event.getItem()),event.getPlayer());
            ItemStack item = event.getItem().clone();
            item.setAmount(item.getAmount() - 1);
            if (item.getAmount() == 0) item = new ItemStack(Material.AIR);
            event.getPlayer().setItemInHand(item);
            event.getPlayer().playSound(event.getClickedBlock().getLocation(), Sound.BLOCK_FENCE_GATE_OPEN, 1, 1);
        }
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.hasItem() && event.getItem().getType() == Material.SIGN && event.getClickedBlock().getType() == Material.END_ROD){
            event.getClickedBlock().getRelative(event.getBlockFace()).setType(Material.WALL_SIGN);
            FoodUtils.decrement(event.getPlayer());
            if (event.getBlockFace() == BlockFace.NORTH)event.getClickedBlock().getRelative(event.getBlockFace()).setData((byte) 2);
            if (event.getBlockFace() == BlockFace.WEST)event.getClickedBlock().getRelative(event.getBlockFace()).setData((byte) 4);
            if (event.getBlockFace() == BlockFace.EAST)event.getClickedBlock().getRelative(event.getBlockFace()).setData((byte) 5);
            if (event.getBlockFace() == BlockFace.SOUTH)event.getClickedBlock().getRelative(event.getBlockFace()).setData((byte) 3);
        }
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() == Material.WALL_SIGN){
            if (event.getPlayer().isSneaking()){
                for (PlayerPortal playerPortal : PlayerPortal.playerPortals){
                    playerPortal.getPortalList().stream().filter(portal -> portal.getSign().equals(event.getClickedBlock().getState())).forEach(portal -> {
                        AnvilGUI anvilGUI = new AnvilGUI(event.getPlayer(), event1 -> {
                            event1.setWillClose(true);
                            event1.setWillDestroy(true);
                            portal.setName(org.bukkit.ChatColor.translateAlternateColorCodes('&',event1.getName()));
                            playerPortal.getPortalList().stream().forEach(Portal::updateSign);
                        },SurvivalMain.getInstance());
                        ItemStack tag = new ItemStackBuilder(Material.SIGN,1).setDisplayName(ChatColor.RESET+"Nombra el portal").build();
                        anvilGUI.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT,tag);
                        anvilGUI.open();
                    });
                    break;
                }
            }else{
                for (PlayerPortal playerPortal : PlayerPortal.playerPortals){
                    playerPortal.getPortalList().stream().filter(portal -> portal.getSign().equals(event.getClickedBlock().getState())).forEach(portal -> {
                        int afterpos = Math.abs((playerPortal.getPortalList().indexOf(portal.getDestination()) + 1) % playerPortal.getPortalList().size());
                        Portal destination = playerPortal.getPortalList().get(afterpos);
                        destination.setDestination(portal);
                        portal.setDestination(destination);
                        destination.updateSign();
                        portal.updateSign();
                    });
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onDropIem(PlayerDropItemEvent event) {
        if (event.getItemDrop().getItemStack().getType().equals(Material.INK_SACK)) {
            event.getItemDrop().getNearbyEntities(4, 4, 4).stream().filter(entity -> entity instanceof Item).forEach(entity -> {
                if (((Item) entity).getItemStack().getAmount() == 4 && ((Item) entity).getItemStack().hasItemMeta() && ((Item) entity).getItemStack().getItemMeta().getDisplayName().equals(PlayerPortal.name)) {
                    ItemStack stack = ((Item) entity).getItemStack();
                    ItemMeta meta = stack.getItemMeta();
                    List<String> lore = meta.getLore();
                    lore.set(0, ChatColor.BLUE + "" + ChatColor.BOLD + "Color: " + ChatColor.DARK_AQUA + (15 - event.getItemDrop().getItemStack().getDurability()));
                    meta.setLore(lore);
                    for (int i = 0; i <= 25; ++i) {
                        entity.getLocation().getWorld().playEffect(entity.getLocation().clone().add(0, 0.5, 0), Effect.MAGIC_CRIT, 10);
                    }
                    entity.getLocation().getWorld().playSound(entity.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                    stack.setItemMeta(meta);
                    event.getItemDrop().remove();
                }
            });
        }
        if (event.getItemDrop().getItemStack().getType().equals(Material.TNT)) {
            for (PlayerPortal playerPortal : PlayerPortal.playerPortals) {
                for (Portal portal : playerPortal.getPortalList()) {
                    if (portal != null && portal.isCompleted() && portal.getPlayersOnPortal(1.5).contains(event.getPlayer()) && playerPortal.getOwner().equals(event.getPlayer().getUniqueId())) {
                        portal.destroy(event.getPlayer());
                        event.getItemDrop().remove();
                        return;
                    }
                }
            }
        }

    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getBlock().hasMetadata("unbreakeable")) {
            event.setCancelled(true);
        }
    }


    public static UUID getIdFromItem(ItemStack stack) {
        return UUID.fromString(stack.getItemMeta().getLore().get(1).replace(ChatColor.BLUE + "" + ChatColor.BOLD + "Id: " + ChatColor.DARK_AQUA, ""));
    }

    public static int getColorFromItem(ItemStack stack) {
        return Integer.parseInt(stack.getItemMeta().getLore().get(0).replace(ChatColor.BLUE + "" + ChatColor.BOLD + "Color: " + ChatColor.DARK_AQUA, ""));
    }
}
