package com.buuz135.survival.portals;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class PlayerPortal {


    public static String name = ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Creador de portal";
    public static List<PlayerPortal> playerPortals = new ArrayList<>();


    public static PlayerPortal getPlayerPortalByPlayer(Player player){
        Optional<PlayerPortal> playerp = playerPortals.stream().filter(playerPortal -> playerPortal.getOwner().equals(player.getUniqueId())).findFirst();
        if (playerp.isPresent()){
            return  playerp.get();
        }
        return new PlayerPortal(player.getUniqueId(),new ArrayList<>());
    }

    public static PlayerPortal getPlayerPortalByPortal(Portal portal){
        Optional<PlayerPortal> playerp = playerPortals.stream().filter(playerPortal -> playerPortal.getPortalList().contains(portal)).findFirst();
        if (playerp.isPresent()){
            return  playerp.get();
        }
        return null;
    }

    private UUID owner;
    private List<Portal> portalList;

    public PlayerPortal(UUID owner, List<Portal> portalList) {
        this.owner = owner;
        this.portalList = portalList;
        playerPortals.add(this);
    }

    public PlayerPortal(JsonObject jsonObject) {
        this.owner = UUID.fromString(jsonObject.get("owner").getAsString());
        this.portalList  = new ArrayList<>();
        for (JsonElement element : jsonObject.getAsJsonArray("portals")){
            portalList.add(new Portal(element.getAsJsonObject()));
        }
        playerPortals.add(this);
    }

    public void addPoint(UUID portal, Location location, int color, Player player){
        Portal p = getPortal(portal);
        if (p == null){
            p = new Portal(portal,location,color);
            portalList.add(p);
        }else{
            p.setPoint2(location);
            p.construct(player, portal);
            portalList.stream().filter(Portal::isCompleted).forEach(Portal::updateSign);
        }
    }

    public Portal getPortal(UUID portalUuid){
        for (Portal portal : portalList){
            if (portal.getUuid().equals(portalUuid)){
                return portal;
            }
        }
        return null;
    }

    public UUID getOwner() {
        return owner;
    }

    public List<Portal> getPortalList() {
        return portalList;
    }

    public JsonElement getJsonObject() {
        JsonObject object = new JsonObject();
        object.addProperty("owner",owner.toString());
        JsonArray por = new JsonArray();
        portalList.forEach(portal -> por.add(portal.getJsonObject()));
        object.add("portals",por);
        return object;
    }
}
