package com.buuz135.survival.portals;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.utils.SerializationUtils;
import com.google.gson.JsonObject;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_9_R2.*;
import net.minecraft.server.v1_9_R2.World;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_9_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.*;

import static org.bukkit.Material.AIR;
import static org.bukkit.Material.WALL_SIGN;

public class Portal {

    private Location point1;
    private Location point2;
    private int colorID;
    private List<EntityArmorStand> entityArmorStands;
    private List<String> playersSee;
    private List<Location> portalArea;
    private HashMap<Player, Long> playersCooldown;
    private Location teleportLocation;
    private UUID uuid;
    private Portal destination;
    private Sign sign;
    private String name;


    public Portal(UUID uuid,Location point1, int colorID) {
        this(uuid, point1, null, colorID);
    }

    public Portal(UUID uuid, Location point1, Location point2, int colorID) {
        this.uuid = uuid;
        this.point1 = point1;
        this.point2 = point2;
        this.colorID = colorID;
        this.entityArmorStands = new ArrayList<>();
        this.playersSee = new ArrayList<>();
        this.portalArea = new ArrayList<>();
        this.playersCooldown = new HashMap<>();
        this.name = "Portal "+ colorID;
        this.destination = this;
    }

    public Portal(JsonObject object) {
        this.entityArmorStands = new ArrayList<>();
        this.playersSee = new ArrayList<>();
        this.portalArea = new ArrayList<>();
        this.playersCooldown = new HashMap<>();
        this.uuid = UUID.fromString(object.get("uuid").getAsString());
        this.point1 = SerializationUtils.deserializeLocation(object.getAsJsonObject("pointFirst"));
        this.point2 = null;
        if (object.has("pointSecond")) {
            this.point2 = SerializationUtils.deserializeLocation(object.getAsJsonObject("pointSecond"));
        }
        this.colorID = object.get("color").getAsInt();
        if (point1 != null && point2 != null) construct(null, null);
        this.name = object.get("name").getAsString();
    }

    public boolean isCompleted() {
        return point2 != null;
    }

    public Location getPoint1() {
        return point1;
    }

    public void setPoint1(Location point1) {
        this.point1 = point1;
    }

    public Location getPoint2() {
        return point2;
    }

    public void setPoint2(Location point2) {
        this.point2 = point2;
    }

    public int getColorID() {
        return colorID;
    }

    public void setColorID(int colorID) {
        this.colorID = colorID;
    }

    public void construct(Player player, UUID id) {
        World w = ((CraftWorld) point1.getWorld()).getHandle();
        for (double y = getSmallY(); y <= getBigY(); ++y) {
            for (double x = getSmall().getX(); x <= getBig().getX(); ++x) {
                for (double z = getSmall().getZ(); z <= getBig().getZ(); ++z) {
                    Location temp = new Location(point1.getWorld(), x, y, z);
                    portalArea.add(temp);
                }
            }
        }
        if (!checkStructure()) {
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Estructura de portal no válida.");
            ItemStack stack = new ItemStack(Material.PAPER, 2);
            ItemMeta meta = stack.getItemMeta();
            meta.setDisplayName(PlayerPortal.name);
            meta.setLore(Arrays.asList(ChatColor.BLUE + "" + ChatColor.BOLD + "Color: " + ChatColor.DARK_AQUA + colorID, ChatColor.BLUE + "" + ChatColor.BOLD + "Id: " + ChatColor.DARK_AQUA + id.toString()));
            stack.setItemMeta(meta);
            player.getInventory().addItem(stack);
            PlayerPortal.getPlayerPortalByPlayer(player).getPortalList().remove(this);
            return;
        }
        for (double y = getSmallY(); y <= getBigY()-0.5; y += 0.62) {
            for (double x = getSmall().getX(); x <= getBig().getX(); x += 0.62) {
                for (double z = getSmall().getZ(); z <= getBig().getZ(); z += 0.62) {
                    double xoff = 0.22;
                    double zoff = 0.76;
                    if (getRotation() == 0) {
                        xoff = 0.76;
                        zoff = 0.78;
                    }
                    EntityArmorStand stand = new EntityArmorStand(w, x + xoff, y - 1.5, z + zoff);
                    stand.setGravity(false);
                    stand.setInvisible(true);
                    stand.setLocation(x + xoff, y - 1.5, z + zoff, getRotation(), 0);
                    entityArmorStands.add(stand);
                }
            }
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(SurvivalMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (player != null) player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
            }
        }, 5);
        for (Location location : portalArea) {
            location.getBlock().setMetadata("unbreakeable", new FixedMetadataValue(SurvivalMain.getInstance(), true));
        }
    }

    private int getRotation() {
        if (point1.getX() == point2.getX()) return 90;
        return 0;
    }

    private Location getBig() {
        if (point1.getX() >= point2.getX() && point1.getZ() >= point2.getZ()) return point1;
        return point2;
    }

    private Location getSmall() {
        if (getBig() == point1) return point2;
        else return point1;
    }

    private int getBigY() {
        if (point1.getY() > point2.getY()) return point1.getBlockY();
        return point2.getBlockY();
    }

    private int getSmallY() {
        if (point1.getY() < point2.getY()) return point1.getBlockY();
        return point2.getBlockY();
    }

    public List<String> getPlayersSee() {
        return playersSee;
    }

    public void checkPlayer(Player player) {
        if (!playersSee.contains(player.getName()) && entityArmorStands.size() > 0 && player.getLocation().distance(point1) <= 64) {
            net.minecraft.server.v1_9_R2.ItemStack stack = new net.minecraft.server.v1_9_R2.ItemStack(Item.getById(160));
            stack.setData(colorID);
            for (EntityArmorStand stand : entityArmorStands) {
                PacketPlayOutSpawnEntity entity = new PacketPlayOutSpawnEntity(stand, 78, 10);
                PacketPlayOutEntityMetadata metadata = new PacketPlayOutEntityMetadata(stand.getId(), stand.getDataWatcher(), true);
                PacketPlayOutEntityEquipment entityEquipment = new PacketPlayOutEntityEquipment(stand.getId(), EnumItemSlot.HEAD, stack);
                EntityPlayer playerOnline = ((CraftPlayer) player).getHandle();
                playerOnline.playerConnection.sendPacket(entity);
                playerOnline.playerConnection.sendPacket(metadata);
                playerOnline.playerConnection.sendPacket(entityEquipment);
            }
            playersSee.add(player.getName());
        }
    }

    public void destroy(Player owner) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            for (EntityArmorStand stand : entityArmorStands) {
                PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(stand.getId());
                EntityPlayer playerOnline = ((CraftPlayer) player).getHandle();
                playerOnline.playerConnection.sendPacket(destroy);
            }
        }
        for (Location location : portalArea) {
            location.getBlock().removeMetadata("unbreakeable", SurvivalMain.getInstance());
            location.getWorld().spawnParticle(Particle.EXPLOSION_NORMAL, location, 1);
        }
        owner.playSound(owner.getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1, 1);
        ItemStack stack = new ItemStack(Material.PAPER, 2);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(PlayerPortal.name);
        meta.setLore(Arrays.asList(ChatColor.BLUE + "" + ChatColor.BOLD + "Color: " + ChatColor.DARK_AQUA + colorID, ChatColor.BLUE + "" + ChatColor.BOLD + "Id: " + ChatColor.DARK_AQUA + uuid.toString()));
        stack.setItemMeta(meta);
        owner.getInventory().addItem(stack);
        PlayerPortal.getPlayerPortalByPlayer(owner).getPortalList().remove(this);
    }

    private boolean checkStructure() {
        for (PlayerPortal playerPortal : PlayerPortal.playerPortals) {
            for (Portal portal : playerPortal.getPortalList()) {
                if (portal != null && portal.isCompleted() && portal != this) {
                    for (Location location : portal.getPortalArea()) {
                        for (Location actual : portalArea) {
                            if (location.getBlock().getType() == AIR && actual.getBlock().getType() == AIR && location.equals(actual))
                                return false;
                        }
                    }
                }
            }
        }
        if (!checkSign()){
            return false;
        }
        if (getSmall().distance(getBig()) != 5.830951894845301) {
            return false;
        }
        for (double y = getSmallY(); y <= getBigY(); ++y) {
            for (double x = getSmall().getX(); x <= getBig().getX(); ++x) {
                for (double z = getSmall().getZ(); z <= getBig().getZ(); ++z) {
                    Location temp = new Location(point1.getWorld(), x, y, z);
                    if (y == getSmallY() || y == getBigY()) {
                        if (!temp.getBlock().getType().name().contains("STEP")) {
                            return false;
                        }
                    } else if ((x == getSmall().getX() || x == getBig().getX()) && (z == getSmall().getZ() || z == getBig().getZ())) {
                        if (temp.getBlock().getType() != Material.END_ROD) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public List<Player> getPlayersOnPortal(double distance) {
        List<Player> players = new ArrayList<>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            for (Location location : portalArea) {
                Location temp = location.clone();
                temp.add(0.5, 0, 0.5);
                if (!players.contains(player) && player.getLocation().distance(temp) < distance) {
                    players.add(player);
                    break;
                }
            }
        }
        return players;
    }

    public Location getLocationToTeleport() {
        if (teleportLocation != null) return teleportLocation;
        for (Location l : portalArea) {
            if (l.getBlock().getY() == getSmallY() + 1 && l.getBlock().getType() == AIR) {
                teleportLocation = l;
                return l;
            }
        }
        return null;
    }

    public HashMap<Player, Long> getPlayersCooldown() {
        return playersCooldown;
    }

    public JsonObject getJsonObject() {
        JsonObject object = new JsonObject();
        object.addProperty("uuid",uuid.toString());
        object.addProperty("color", colorID);
        object.addProperty("name",name);
        object.add("pointFirst", SerializationUtils.serializeLocation(point1));
        if (point2 != null) object.add("pointSecond", SerializationUtils.serializeLocation(point2));
        return object;
    }

    public List<Location> getPortalArea() {
        return portalArea;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Portal getDestination() {
        return destination;
    }

    public void setDestination(Portal destination) {
        this.destination = destination;
    }

    public boolean checkSign(){
        for (Location location : portalArea){
            for (BlockFace face : new BlockFace[]{BlockFace.EAST,BlockFace.NORTH,BlockFace.SOUTH,BlockFace.WEST}){
                if (location.getBlock().getType() != AIR && !portalArea.contains(location.getBlock().getRelative(face).getLocation()) && location.getBlock().getRelative(face).getType() == WALL_SIGN){
                    sign = (Sign) location.getBlock().getRelative(face).getState();
                    return true;
                }
            }
        }
        return false;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Sign getSign() {
        return sign;
    }

    public void updateSign(){
        PlayerPortal playerPortal = PlayerPortal.getPlayerPortalByPortal(this);
        if (playerPortal == null) return;
        int currentpos = playerPortal.getPortalList().indexOf(destination);
        int beforepos = Math.floorMod((currentpos-1),playerPortal.getPortalList().size());
        int afterpos = Math.floorMod((currentpos+1),playerPortal.getPortalList().size());
        sign.setLine(0,ChatColor.BLUE+"Destinación:");
        sign.setLine(1,playerPortal.getPortalList().get(beforepos).getName());
        sign.setLine(2,ChatColor.BOLD+" > " +ChatColor.RESET + destination.getName()+ChatColor.BLACK+ChatColor.BOLD+ " < ");
        sign.setLine(3,playerPortal.getPortalList().get(afterpos).getName());
        sign.update();
    }
}
