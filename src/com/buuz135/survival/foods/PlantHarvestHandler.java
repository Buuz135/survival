package com.buuz135.survival.foods;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.utils.FoodUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;


public class PlantHarvestHandler implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) return;
        if (event.getBlock().getType().equals(Material.LONG_GRASS)) {
            Random rn = new Random();
            if (rn.nextBoolean() && rn.nextBoolean() && rn.nextBoolean()) {
                event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), FoodUtils.getRandomCropDrop(rn));
            }
        }
        if (SurvivalMain.getInstance().getCropStorageManager().getCropLocations().containsKey(event.getBlock().getLocation())) {
            FoodPlant foodPlant = FoodUtils.getFoodPlantFromInt(SurvivalMain.getInstance().getCropStorageManager().getCropLocations().get(event.getBlock().getLocation()));
            SurvivalMain.getInstance().getCropStorageManager().removeCrop(event.getBlock().getLocation());
            event.setCancelled(true);
            event.getBlock().getLocation().getWorld().playSound(event.getBlock().getLocation(), Sound.BLOCK_GRASS_BREAK, 1, 1);
            ItemStack stack = foodPlant.getItem().clone();
            if (event.getBlock().getData() == (byte) 7) {
                stack.setAmount(1 + new Random().nextInt(foodPlant.getMaxDrop()));
            } else {
                stack.setAmount(1);
            }
            event.getBlock().setType(Material.AIR);
            event.getBlock().getLocation().getWorld().dropItemNaturally(event.getBlock().getLocation(), stack);

        }
    }

    @EventHandler
    public void onInteractBlock(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.hasItem() && event.getClickedBlock().getType() == Material.SOIL && event.getBlockFace() == BlockFace.UP &&
                FoodUtils.getFoodFromItemStack(event.getPlayer().getItemInHand()) != null) {
            event.setCancelled(true);
            FoodPlant foodPlant = (FoodPlant) FoodUtils.getFoodFromItemStack(event.getPlayer().getItemInHand());
            if (foodPlant != null) {
                Block block = event.getClickedBlock().getRelative(BlockFace.UP);
                block.setType(Material.CARROT);
                FoodUtils.decrement(event.getPlayer());
                SurvivalMain.getInstance().getCropStorageManager().addCrop(block.getLocation(), foodPlant);
            }
        }
        if (event.getPlayer().getItemInHand().getType() == Material.DIAMOND_HOE) {
            Block block = event.getClickedBlock();
            if (SurvivalMain.getInstance().getCropStorageManager().getCropLocations().containsKey(block.getLocation())) {
                FoodPlant food = FoodUtils.getFoodPlantFromInt(SurvivalMain.getInstance().getCropStorageManager().getCropLocations().get(block.getLocation()));
                event.getPlayer().sendMessage(ChatColor.DARK_AQUA + " Cultivo: " + ChatColor.GOLD + food.getName());
                event.getPlayer().sendMessage(ChatColor.DARK_AQUA + " Crecimiento: " + ChatColor.GOLD + block.getData() + ChatColor.GRAY + "/" + ChatColor.GOLD + 7);
                event.getPlayer().sendMessage(ChatColor.DARK_AQUA + " Min. : " + ChatColor.GOLD + food.getMinDrop());
                event.getPlayer().sendMessage(ChatColor.DARK_AQUA + " Max. : " + ChatColor.GOLD + food.getMaxDrop());
            }
        }
    }

    @EventHandler
    public void onLeaveDacay(LeavesDecayEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(SurvivalMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                Random rn = new Random();
                if (rn.nextBoolean() && rn.nextBoolean() && rn.nextBoolean()) {
                    event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), FoodUtils.getRandomTreeDrop(rn));
                }
            }
        }, 20);
    }

}
