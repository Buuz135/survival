package com.buuz135.survival.foods;

import com.buuz135.survival.utils.FoodUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Random;


public class EatHandler implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR)
                && event.getPlayer().getItemInHand().getType() != Material.AIR) {
            eatOnHand(event.getPlayer(), event);
        }
    }


    @EventHandler
    public void onInteractEnt(PlayerInteractAtEntityEvent event) {
        if (event.getPlayer().getItemInHand().getType() != Material.AIR) {
            eatOnHand(event.getPlayer(), event);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlace(BlockPlaceEvent event) {
        if (FoodUtils.getFoodFromItemStack(event.getPlayer().getItemInHand()) != null) {
            Bukkit.broadcastMessage(String.valueOf(FoodUtils.getFoodFromItemStack(event.getPlayer().getItemInHand())));
            event.setCancelled(true);
        }
    }

    private void eatOnHand(Player player, Cancellable cancellable) {
        if (player.getFoodLevel() < 20) {
            IFood foodPlant = FoodUtils.getFoodFromItemStack(player.getItemInHand());
            if (foodPlant != null) {
                cancellable.setCancelled(true);
                FoodUtils.decrement(player);
                player.setFoodLevel((int) (player.getFoodLevel() + foodPlant.getFood()));
                player.setSaturation((float) (player.getSaturation() + foodPlant.getSaturation()));
                player.getWorld().playSound(player.getLocation(), Sound.ENTITY_GENERIC_EAT, 1, (float) (1 + new Random().nextDouble()));
            }
        }
    }


}
