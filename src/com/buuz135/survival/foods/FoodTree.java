package com.buuz135.survival.foods;

import com.buuz135.survival.utils.SkullUtils;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class FoodTree implements IFood {

    public static List<FoodTree> foodTreeList = new ArrayList<>();


    private ItemStack item;
    private String name;
    private int id;
    private double food;
    private double saturation;
    private String UUID;

    public FoodTree(String name, String encodedURL, double food, double saturation, String uuid) {
        this.name = name;
        item = SkullUtils.createItemSkull(SkullUtils.getNonPlayerProfile(uuid, encodedURL));
        id = foodTreeList.size();
        this.food = food;
        this.saturation = saturation;
        this.UUID = uuid;
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + name);
        List<String> stringList = new ArrayList<>();
        stringList.add(ChatColor.DARK_AQUA + "Comida: " + ChatColor.GOLD + food);
        stringList.add(ChatColor.DARK_AQUA + "Saturación: " + ChatColor.GOLD + saturation);
        meta.setLore(stringList);
        item.setItemMeta(meta);
        foodTreeList.add(this);
    }


    public ItemStack getItem() {
        return item;
    }

    public double getFood() {
        return food;
    }

    public double getSaturation() {
        return saturation;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUUID() {
        return UUID;
    }

}
