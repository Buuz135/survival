package com.buuz135.survival.foods;


import org.bukkit.inventory.ItemStack;

public interface IFood {

    public String getName();

    public String getUUID();

    public ItemStack getItem();

    public double getFood();

    public double getSaturation();

}
