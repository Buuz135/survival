package com.buuz135.survival.survival;

import com.buuz135.survival.hologram.ShinyHologram;
import com.buuz135.survival.utils.LocationUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SurvivalUtilsHandler implements Listener {


    private static String line1 = ChatColor.BLUE + "" + ChatColor.BOLD + "[SURVIVAL]";
    private static String line2 = ChatColor.RED + "TP RANDOM";

    @EventHandler
    public void onSignCreate(SignChangeEvent event) {
        if (event.getPlayer().isOp()) {
            if (event.getLine(0).equalsIgnoreCase("[SURVIVAL]")) {
                if (event.getLine(1).equalsIgnoreCase("random")) {
                    int size = Integer.parseInt(event.getLine(2));
                    event.setLine(0, line1);
                    event.setLine(1, line2);
                    event.setLine(2, ChatColor.GREEN + "Radio: " + size);
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (event.getClickedBlock().getType() == Material.WALL_SIGN) {
                Sign sign = (Sign) event.getClickedBlock().getState();
                if (sign.getLine(0).equals(line1)) {
                    if (sign.getLine(1).equals(line2)) {
                        int size = Integer.parseInt(sign.getLine(2).split(" ")[1]);
                        Location location = LocationUtils.getRandomLocationFromOutside(event.getPlayer().getLocation(), size, 0);
                        if (location != null) {
                            event.getPlayer().teleport(location);
                        }
                    }
                }
            }
        }
        if (event.getItem().getType() == Material.STICK && event.getAction() == Action.RIGHT_CLICK_AIR){
            List<String> stringList = new ArrayList<>();
            stringList.add("Line 1");
            stringList.add("Line 2");
            new ShinyHologram(stringList, event.getPlayer().getLocation(),0);
        }
    }

}
