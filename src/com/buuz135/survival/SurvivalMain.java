package com.buuz135.survival;

import com.buuz135.survival.chunk.SurvivalChunkHandler;
import com.buuz135.survival.flag.FlagEventHandler;
import com.buuz135.survival.foods.EatHandler;
import com.buuz135.survival.foods.PlantHarvestHandler;
import com.buuz135.survival.hologram.HologramHandler;
import com.buuz135.survival.jobs.JobsHandler;
import com.buuz135.survival.managers.*;
import com.buuz135.survival.portals.PortalHandler;
import com.buuz135.survival.protectedarea.SurvivalPlotHandler;
import com.buuz135.survival.radar.MapRadarHandler;
import com.buuz135.survival.shop.ShopEventHandler;
import com.buuz135.survival.survival.SurvivalUtilsHandler;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class SurvivalMain extends JavaPlugin implements Listener {

    private static SurvivalMain instance;
    public static final String TAG = ChatColor.DARK_PURPLE + "Dr. Quark" + ChatColor.GRAY + " » ";
    private DataManager dataManager;
    private EconomyManager economyManager;
    private EventManager eventManager;
    private CommandManager commandManager;
    private TaskManager taskManager;
    private RecipeManager recipeManager;
    private ChunkManager chunkManager;
    private JobsManager jobsManager;
    private CropStorageManager cropStorageManager;
    private FoodManager foodManager;

    @Override
    public void onEnable() {
        super.onEnable();
        this.instance = this;
        File folder = new File("survival");
        if (!folder.exists()) {
            folder.mkdir();
        }
        try {
            dataManager = new DataManager(folder);
            dataManager.onEnable();
        } catch (IOException e) {
            e.printStackTrace();
        }
        economyManager = new EconomyManager();
        eventManager = new EventManager(this);
        Listener[] listeners = new Listener[]{new FlagEventHandler(), new PortalHandler(), new ShopEventHandler(), new MapRadarHandler(), new SurvivalPlotHandler(), new HologramHandler(), new SurvivalUtilsHandler(), new SurvivalChunkHandler(), new JobsHandler(), new EatHandler(), new PlantHarvestHandler()};
        for (Listener l : listeners) eventManager.registerEvent(l);
        commandManager = new CommandManager(this);
        taskManager = new TaskManager(this);
        taskManager.onEnable();
        recipeManager = new RecipeManager();
        chunkManager = new ChunkManager(folder);
        chunkManager.onEnable();
        jobsManager = new JobsManager();
        jobsManager.onEnable();
        foodManager = new FoodManager();
        cropStorageManager = new CropStorageManager(folder);
        cropStorageManager.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
        dataManager.onDisable();
        taskManager.onDisable();
        chunkManager.onDisable();
        jobsManager.onDisable();
        cropStorageManager.onDisable();
    }

    public static SurvivalMain getInstance() {
        return instance;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public EconomyManager getEconomyManager() {
        return economyManager;
    }

    public ChunkManager getChunkManager() {
        return chunkManager;
    }

    public JobsManager getJobsManager() {
        return jobsManager;
    }

    public CropStorageManager getCropStorageManager() {
        return cropStorageManager;
    }
}
