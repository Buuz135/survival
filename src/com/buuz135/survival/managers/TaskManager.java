package com.buuz135.survival.managers;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.hologram.Hologram;
import com.buuz135.survival.jobs.IBasicJob;
import com.buuz135.survival.jobs.WorkingPlayer;
import com.buuz135.survival.portals.PlayerPortal;
import com.buuz135.survival.portals.Portal;
import com.buuz135.survival.radar.MapRadar;
import com.buuz135.survival.shop.Shop;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_9_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TaskManager {


    private JavaPlugin javaPlugin;

    private BukkitTask portalRenderer;
    private BukkitTask portalTeleporter;
    private BukkitTask shopRenderer;
    private BukkitTask mapRenderer;
    private BukkitTask hologramRenderer;
    private BukkitTask jobsScoreboard;

    private Runnable portalRenderRunnable;
    private Runnable portalTeleportRunnable;
    private Runnable shopDisplayRunnable;
    private Runnable mapDisplayRunnable;
    private Runnable hologramRenderRunnable;
    private Runnable jobsScoreboardRunnable;

    public TaskManager(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        setupRunnables();
    }

    public void onEnable() {
        portalRenderer = Bukkit.getScheduler().runTaskTimerAsynchronously(javaPlugin, portalRenderRunnable, 2 * 20, 2 * 20);
        portalTeleporter = Bukkit.getScheduler().runTaskTimer(javaPlugin, portalTeleportRunnable, 20, 20);
        shopRenderer = Bukkit.getScheduler().runTaskTimerAsynchronously(javaPlugin, shopDisplayRunnable, 20, 20);
        mapRenderer = Bukkit.getScheduler().runTaskTimerAsynchronously(javaPlugin, mapDisplayRunnable, 2, 2);
        hologramRenderer = Bukkit.getScheduler().runTaskTimerAsynchronously(javaPlugin, hologramRenderRunnable, 2 * 20, 2 * 20);
        jobsScoreboard = Bukkit.getScheduler().runTaskTimerAsynchronously(javaPlugin, jobsScoreboardRunnable, 20, 20);
    }

    public void onDisable() {
        portalRenderer.cancel();
        portalTeleporter.cancel();
        shopRenderer.cancel();
        mapRenderer.cancel();
        hologramRenderer.cancel();
        jobsScoreboard.cancel();
    }

    private void setupRunnables() {
        portalRenderRunnable = new Runnable() {
            @Override
            public void run() {
                for (PlayerPortal portal : PlayerPortal.playerPortals) {
                    for (Player online : Bukkit.getOnlinePlayers()) {
                        for (Portal p : portal.getPortalList()){
                            p.checkPlayer(online);
                            if (online.getLocation().distance(p.getPoint1()) >= 64 && p.getPlayersSee().contains(online.getName()))
                                p.getPlayersSee().remove(online.getName());
                        }
                    }
                    for (Portal p : portal.getPortalList()){
                        List<String> remove = p.getPlayersSee().stream().filter(name -> Bukkit.getPlayer(name) == null).collect(Collectors.toList());
                        for (String name : remove) p.getPlayersSee().remove(name);
                    }
                }
            }
        };
        portalTeleportRunnable = new Runnable() {
            @Override
            public void run() {
                PlayerPortal.playerPortals.stream().forEach(playerPortal -> playerPortal.getPortalList().stream().filter(Portal::isCompleted).forEach(portal -> {
                    List<Player> playerList = new ArrayList<>();
                    portal.getPlayersCooldown().keySet().stream().filter(player -> 10 * 1000 + portal.getPlayersCooldown().get(player) < new Date().getTime()).forEach(playerList::add);
                    playerList.stream().forEach(player -> portal.getPlayersCooldown().remove(player));
                    if (portal.getDestination() != null && portal.getDestination().isCompleted()){
                        Portal destination = portal.getDestination();
                        portal.getPlayersOnPortal(0.55).stream().filter(player -> !portal.getPlayersCooldown().containsKey(player)).forEach(player -> teleportAndEffect(player,destination));
                    }
                }));
            }
        };
        shopDisplayRunnable = new Runnable() {
            @Override
            public void run() {
                for (Shop s : Shop.shopList) {
                    for (Player online : Bukkit.getOnlinePlayers()) {
                        if (!s.getPlayersSee().contains(online.getName())) {
                            EntityItem item = s.getDisplay();
                            PacketPlayOutSpawnEntity entity = new PacketPlayOutSpawnEntity(item, 2, 10);
                            PacketPlayOutEntityMetadata metadata = new PacketPlayOutEntityMetadata(item.getId(), item.getDataWatcher(), true);
                            PacketPlayOutEntityVelocity velocity = new PacketPlayOutEntityVelocity(item.getId(), 0, 0, 0);
                            EntityPlayer player = ((CraftPlayer) online).getHandle();
                            player.playerConnection.sendPacket(entity);
                            player.playerConnection.sendPacket(metadata);
                            player.playerConnection.sendPacket(velocity);
                            s.getPlayersSee().add(online.getName());
                        }
                        if (online.getLocation().distance(s.getLocation()) >= 32 && s.getPlayersSee().contains(online.getName()))
                            s.getPlayersSee().remove(online.getName());
                    }
                    List<String> remove = s.getPlayersSee().stream().filter(name -> Bukkit.getPlayer(name) == null).collect(Collectors.toList());
                    for (String name : remove) {
                        s.getPlayersSee().remove(name);
                    }
                }

            }
        };
        mapDisplayRunnable = new Runnable() {
            @Override
            public void run() {
                MapRadar.mapRadars.stream().filter(map -> Bukkit.getOfflinePlayer(map.getPlayer()).isOnline() && map.isVisible()).forEach(map -> map.getBar().setTitle(map.getName()));
            }
        };
        hologramRenderRunnable = new Runnable() {
            @Override
            public void run() {
                for (Hologram hologram : Hologram.hologramList) {
                    for (Player online : Bukkit.getOnlinePlayers()) {
                        if (!hologram.getPlayersCanSee().contains(online)) {
                            EntityArmorStand armorStand = hologram.getArmorStand();
                            PacketPlayOutSpawnEntity entity = new PacketPlayOutSpawnEntity(armorStand, 78, 10);
                            PacketPlayOutEntityMetadata metadata = new PacketPlayOutEntityMetadata(armorStand.getId(), armorStand.getDataWatcher(), true);
                            EntityPlayer player = ((CraftPlayer) online).getHandle();
                            player.playerConnection.sendPacket(entity);
                            player.playerConnection.sendPacket(metadata);
                            hologram.getPlayersCanSee().add(online);
                        }
                        if (online.getLocation().distance(hologram.getLocation()) >= 32 && hologram.getPlayersCanSee().contains(online))
                            hologram.getPlayersCanSee().remove(online);
                    }
                    List<Player> remove = hologram.getPlayersCanSee().stream().filter(player -> !player.isOnline()).collect(Collectors.toList());
                    for (Player player : remove) {
                        hologram.getPlayersCanSee().remove(player);
                    }
                }
            }
        };
        jobsScoreboardRunnable = new Runnable() {
            @Override
            public void run() {
                boolean newDay = false;
                if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) != SurvivalMain.getInstance().getJobsManager().getDay()) {
                    newDay = true;
                    SurvivalMain.getInstance().getJobsManager().setDay(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                }
                for (IBasicJob basicJob : SurvivalMain.getInstance().getJobsManager().getJobs()) {
                    List<WorkingPlayer> remove = basicJob.getWorkingPlayers().stream().filter(player -> new Date(player.getContracted().getTime() + basicJob.getWorkingDays() * 24 * 60 * 60 * 1000).getTime() - new Date().getTime() <= 0).collect(Collectors.toList());
                    basicJob.getWorkingPlayers().stream().filter(player -> !remove.contains(player)).filter(player -> player.getTotalEnergy() >= basicJob.getTotalEnergy()).forEach(player -> {
                        SurvivalMain.getInstance().getEconomyManager().getEconomy().depositPlayer(Bukkit.getOfflinePlayer(player.getPlayer()), basicJob.getPay());
                        remove.add(player);
                        OfflinePlayer off = Bukkit.getOfflinePlayer(player.getPlayer());
                        if (off.isOnline()) {
                            off.getPlayer().sendMessage(SurvivalMain.TAG + ChatColor.GREEN + "¡Has acabado el trabajo y has cobrado por ello!");
                        }
                    });
                    for (WorkingPlayer player : remove) {
                        basicJob.getWorkingPlayers().remove(player);
                    }
                }
                for (Player player : Bukkit.getOnlinePlayers()) {
                    org.bukkit.scoreboard.Scoreboard board = player.getScoreboard();
                    if (board == null) board = Bukkit.getScoreboardManager().getNewScoreboard();
                    IBasicJob basicJob = SurvivalMain.getInstance().getJobsManager().getPlayerJob(player);
                    if (basicJob == null) {
                        board.clearSlot(DisplaySlot.SIDEBAR);
                        player.setScoreboard(board);
                    } else {
                        for (String s : board.getEntries()) {
                            board.resetScores(s);
                        }
                        WorkingPlayer workingPlayer = SurvivalMain.getInstance().getJobsManager().getWorkingPlayer(player);
                        if (newDay) workingPlayer.setEnergy(0);
                        Objective objective = board.getObjective(ChatColor.GOLD + "- " + basicJob.getName() + " -");
                        if (objective == null) {
                            objective = board.registerNewObjective(ChatColor.GOLD + "- " + basicJob.getName() + " -", "dummy");
                        }
                        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
                        setScore(objective, "", 16);
                        setScore(objective, ChatColor.GOLD + "Energía diaria: ", 15);
                        setScore(objective, ChatColor.AQUA + " " + new DecimalFormat("#0.00").format(workingPlayer.getEnergy()) + ChatColor.WHITE + "/" + ChatColor.AQUA + new DecimalFormat("#0.00").format(basicJob.getMaxEnergy()), 14);
                        setScore(objective, ChatColor.GOLD + "Energía total: ", 13);
                        setScore(objective, ChatColor.AQUA + " " + new DecimalFormat("#0.00").format(workingPlayer.getTotalEnergy()) + ChatColor.WHITE + "/" + ChatColor.AQUA + new DecimalFormat("#0.00").format(basicJob.getTotalEnergy()), 12);
                        long date = new Date(workingPlayer.getContracted().getTime() + basicJob.getWorkingDays() * 24 * 60 * 60 * 1000).getTime() - new Date().getTime();
                        setScore(objective, ChatColor.GOLD + "Tiempo restante: ", 11);
                        setScore(objective, ChatColor.AQUA + " " + formatDate(date), 10);
                        setScore(objective, ChatColor.GOLD + "Reinicio diario:", 9);
                        Calendar c = Calendar.getInstance();
                        c.add(Calendar.DAY_OF_MONTH, 1);
                        c.set(Calendar.HOUR_OF_DAY, 0);
                        c.set(Calendar.MINUTE, 0);
                        c.set(Calendar.SECOND, 0);
                        c.set(Calendar.MILLISECOND, 0);
                        long diary = (c.getTimeInMillis() - System.currentTimeMillis());
                        setScore(objective, ChatColor.AQUA + " " + formatDate(diary), 8);
                        setScore(objective, ChatColor.GOLD + "", 7);
                        setScore(objective, ChatColor.GOLD + "mc.corsariocraft.net", 6);
                    }
                }
            }
        };
    }


    private void teleportAndEffect(Player player, Portal portal) {
        Location location = portal.getLocationToTeleport().clone();
        location.add(0.5,0,0.5);
        location.setYaw(player.getLocation().getYaw());
        location.setPitch(player.getLocation().getPitch());
        player.teleport(location);
        player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 140, 2));
        player.playSound(player.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, 0.5f, 1);
        portal.getPlayersCooldown().put(player, new Date().getTime());
    }


    private void setScore(Objective objective, String name, int pos) {
        Score score = objective.getScore(name);
        score.setScore(pos);
    }

    private String formatDate(long time) {//DEMASIADO TARDE PARA PENSAR DECENTEMENTE
        time = time / 1000;
        int days = (int) (time / 86400);
        time = time % 86400;
        int hours = (int) (time / 3600);
        time = time % 3600;
        int min = (int) (time / 60);
        int seconds = (int) (time % 60);
        String format = formatInt(hours) + ":" + formatInt(min) + ":" + formatInt(seconds);
        if (days > 0) format = days + "d " + format;
        return format;
    }

    private String formatInt(int i) {
        return new DecimalFormat("00").format(i);
    }
}