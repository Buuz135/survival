package com.buuz135.survival.managers;

import com.buuz135.survival.chunk.SurvivalChunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChunkManager {

    private List<SurvivalChunk> survivalChunkList;
    private File folder;
    private List<Material> materials;


    public ChunkManager(File survivalFolder) {
        survivalChunkList = new ArrayList<>();
        folder = new File(survivalFolder.getPath() + File.separator + "chunks");
        materials = new ArrayList<>();
        if (!folder.exists()) folder.mkdir();
    }

    public void onEnable() {
        for (File file : folder.listFiles()) {
            int x = Integer.parseInt(file.getName().split("#")[0]);
            int z = Integer.parseInt(file.getName().split("#")[1].replace(".yml", ""));
            survivalChunkList.add(new SurvivalChunk(x, z, folder));
        }
    }


    public void onDisable() {

    }

    public SurvivalChunk getChunk(int x, int z) {
        for (SurvivalChunk chunk : survivalChunkList) {
            if (chunk.getX() == x && chunk.getZ() == z) return chunk;
        }
        return new SurvivalChunk(x, z, folder);
    }

    public SurvivalChunk getChunk(Location location) {
        return getChunk(location.getChunk().getX(), location.getChunk().getZ());
    }

    public List<Material> getMaterials() {
        return materials;
    }

    public boolean isPlayerPlaced(Block block) {
        try {
            return getChunk(block.getLocation()).exists(block.getLocation());
        } catch (IOException | InvalidConfigurationException e) {
            return true;
        }

    }
}
