package com.buuz135.survival.managers;

import com.buuz135.survival.foods.FoodPlant;
import com.buuz135.survival.utils.FoodUtils;
import com.google.gson.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class CropStorageManager {

    private HashMap<Location, Integer> cropLocations;
    private File storage;
    private File folder;


    public CropStorageManager(File folder) {
        this.folder = folder;
        storage = new File(folder.getPath() + File.separator + "cropStorage.json");
        cropLocations = new HashMap<>();
        if (!storage.exists()) try {
            storage.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onEnable() {
        try {
            JsonArray array = new JsonParser().parse(new FileReader(storage)).getAsJsonArray();
            Iterator<JsonElement> iterator = array.iterator();
            while (iterator.hasNext()) {
                JsonObject object = iterator.next().getAsJsonObject();
                String place = object.get("location").getAsString();
                FoodPlant plant = (FoodPlant) FoodUtils.getFoodFromUUID(object.get("UUID").getAsString());
                if (plant != null) {
                    Location location = new Location(Bukkit.getWorld(place.split(":")[0]), Integer.parseInt(place.split(":")[1]), Integer.parseInt(place.split(":")[2]), Integer.parseInt(place.split(":")[3]));
                    cropLocations.put(location, plant.getId());
                }
            }
        } catch (Exception e) {
        }

    }

    public void onDisable() {
        JsonArray array = new JsonArray();
        for (Location location : cropLocations.keySet()) {
            JsonObject object = new JsonObject();
            object.addProperty("location", location.getWorld().getName() + ":" + location.getBlockX() + ":" + location.getBlockY() + ":" + location.getBlockZ());
            String uuid = FoodUtils.getFoodPlantFromInt(cropLocations.get(location)).getUUID();
            object.addProperty("UUID", uuid);
            array.add(object);
        }
        writeJson(storage, array);
    }

    private void writeJson(File f, JsonElement element) {
        try {
            FileOutputStream outputStream = new FileOutputStream(f);
            Gson gs = new GsonBuilder().setPrettyPrinting().create();
            outputStream.write(gs.toJson(element).getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addCrop(Location location, FoodPlant plant) {
        cropLocations.put(location, plant.getId());
        onDisable();
    }

    public void removeCrop(Location location) {
        if (cropLocations.containsKey(location)) {
            cropLocations.remove(location);
            onDisable();
        }
    }

    public HashMap<Location, Integer> getCropLocations() {
        return cropLocations;
    }
}
