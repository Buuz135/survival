package com.buuz135.survival.managers;

import com.buuz135.survival.jobs.*;
import com.buuz135.survival.utils.ItemStackBuilder;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JobsManager {

    private List<IBasicJob> jobs;
    private int day;

    public JobsManager() { // TODO CARGAR DE LA CONFIG
        this.jobs = new ArrayList<>();
        this.day = 0;
    }

    public void onEnable() {
        jobs.add(new WoodChopper());
        jobs.add(new Fisher());
        jobs.add(new Farmer());
        jobs.add(new Hunter());
        jobs.add(new Miner());
        jobs.add(new Enchanter());
        jobs.add(new Assassin());
    }

    public void onDisable() {

    }

    public Inventory getJobsInventory() {
        Inventory inventory = Bukkit.createInventory(null, 18, "Trabajos");
        for (IBasicJob job : jobs) {
            ItemStack stack = new ItemStackBuilder(Material.EMPTY_MAP).setDisplayName(ChatColor.DARK_AQUA + job.getName()).setLore(Arrays.asList("", job.getDescription(), "",
                    ChatColor.GOLD + "Energía diaria: " + ChatColor.YELLOW + job.getMaxEnergy(),
                    ChatColor.GOLD + "Energía total: " + ChatColor.YELLOW + job.getTotalEnergy(),
                    ChatColor.GOLD + "Contratos restantes: " + ChatColor.YELLOW + (job.getMaxWorkers() - job.getWorkingPlayers().size()) + "/" + job.getMaxWorkers(),
                    ChatColor.GOLD + "Salario: " + ChatColor.YELLOW + job.getPay())).build();
            ItemMeta meta = stack.getItemMeta();
            meta.addItemFlags(ItemFlag.values());
            inventory.addItem(stack);
        }
        return inventory;
    }

    public void fireEvent(Event event) {
        for (IBasicJob job : jobs) job.onEvent(event);
    }

    public List<IBasicJob> getJobs() {
        return jobs;
    }


    public IBasicJob getPlayerJob(Player player) {
        for (IBasicJob job : jobs) {
            for (WorkingPlayer workingPlayer : job.getWorkingPlayers()) {
                if (workingPlayer.getPlayer().equals(player.getUniqueId())) return job;
            }
        }
        return null;
    }

    public WorkingPlayer getWorkingPlayer(Player player) {
        for (IBasicJob job : jobs) {
            for (WorkingPlayer workingPlayer : job.getWorkingPlayers()) {
                if (workingPlayer.getPlayer().equals(player.getUniqueId())) return workingPlayer;
            }
        }
        return null;
    }

    public WorkingPlayer getWorkingPlayerFromJob(Player player, IBasicJob basicJob) {
        for (WorkingPlayer workingPlayer : basicJob.getWorkingPlayers()) {
            if (workingPlayer.getPlayer().equals(player.getUniqueId())) return workingPlayer;
        }

        return null;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
