package com.buuz135.survival.managers;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class EventManager {

    private JavaPlugin instance;

    public EventManager(JavaPlugin instance) {
        this.instance = instance;
    }

    public void registerEvent(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, instance);
    }
}
