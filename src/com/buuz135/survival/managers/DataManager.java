package com.buuz135.survival.managers;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.hologram.Hologram;
import com.buuz135.survival.jobs.IBasicJob;
import com.buuz135.survival.jobs.WorkingPlayer;
import com.buuz135.survival.portals.PlayerPortal;
import com.buuz135.survival.protectedarea.SurvivalPlot;
import com.buuz135.survival.radar.MapRadar;
import com.buuz135.survival.shop.Shop;
import com.google.gson.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public class DataManager {

    private File survivalPlot;
    private File shop;
    private File portal;
    private File map;
    private File hologram;
    private File jobs;

    public DataManager(File folder) throws IOException {
        survivalPlot = new File(folder.getPath() + File.separator + "Plot.json");
        if (!survivalPlot.exists()) survivalPlot.createNewFile();
        shop = new File(folder.getPath() + File.separator + "Shop.json");
        if (!shop.exists()) shop.createNewFile();
        portal = new File(folder.getPath() + File.separator + "Portal.json");
        if (!portal.exists()) portal.createNewFile();
        map = new File(folder.getPath() + File.separator + "Map.json");
        if (!map.exists()) map.createNewFile();
        hologram = new File(folder.getPath() + File.separator + "Hologram.json");
        if (!hologram.exists()) hologram.createNewFile();
        jobs = new File(folder.getPath() + File.separator + "Jobs.json");
        if (!jobs.exists()) jobs.createNewFile();
    }

    public void onEnable() {
        //PLOT
        try {
            JsonArray array = new JsonParser().parse(new FileReader(survivalPlot)).getAsJsonArray();
            for (JsonElement anArray : array) {
                new SurvivalPlot(anArray.getAsJsonObject());
            }
        } catch (Exception e) {
            if (!(e instanceof IllegalStateException)) e.printStackTrace();
        }
        //SHOP
        try {
            JsonArray array = new JsonParser().parse(new FileReader(shop)).getAsJsonArray();
            for (JsonElement anArray : array) {
                new Shop(anArray.getAsJsonObject());
            }
        } catch (Exception e) {
            if (!(e instanceof IllegalStateException)) e.printStackTrace();
        }
        //PORTAL
        try {
            JsonArray array = new JsonParser().parse(new FileReader(portal)).getAsJsonArray();
            for (JsonElement anArray : array) {
                new PlayerPortal(anArray.getAsJsonObject());
            }
        } catch (Exception e) {
            if (!(e instanceof IllegalStateException)) e.printStackTrace();
        }
        //MAP
        try {
            JsonArray array = new JsonParser().parse(new FileReader(map)).getAsJsonArray();
            for (JsonElement anArray : array) {
                new MapRadar(anArray.getAsJsonObject());
            }
        } catch (Exception e) {
            if (!(e instanceof IllegalStateException)) e.printStackTrace();
        }
        //HOLOGRAM
        try {
            JsonArray array = new JsonParser().parse(new FileReader(hologram)).getAsJsonArray();
            for (JsonElement anArray : array) {
                new Hologram(anArray.getAsJsonObject());
            }
        } catch (Exception e) {
            if (!(e instanceof IllegalStateException)) e.printStackTrace();
        }

    }

    public void onDisable() {
        //PLOT
        JsonArray array = new JsonArray();
        for (SurvivalPlot plot : SurvivalPlot.survivalPlots) {
            array.add(plot.getJsonObject());
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        writeString(gson.toJson(array), survivalPlot);
        //SHOP
        array = new JsonArray();
        for (Shop s : Shop.shopList) {
            array.add(s.getJsonObject());
            s.removeFakeItem();
        }
        gson = new GsonBuilder().setPrettyPrinting().create();
        writeString(gson.toJson(array), shop);
        //PORTAL
        array = new JsonArray();
        for (PlayerPortal portal : PlayerPortal.playerPortals) {
            array.add(portal.getJsonObject());
        }
        gson = new GsonBuilder().setPrettyPrinting().create();
        writeString(gson.toJson(array), portal);
        //MAP
        array = new JsonArray();
        for (MapRadar map : MapRadar.mapRadars) {
            array.add(map.getJsonObject());
        }
        gson = new GsonBuilder().setPrettyPrinting().create();
        writeString(gson.toJson(array), map);
        //HOLOGRAM
        array = new JsonArray();
        for (Hologram hologram : Hologram.hologramList) {
            array.add(hologram.getJsonObject());
        }
        gson = new GsonBuilder().setPrettyPrinting().create();
        writeString(gson.toJson(array), hologram);
        //JOBS
        array = new JsonArray();
        for (IBasicJob basicJob : SurvivalMain.getInstance().getJobsManager().getJobs()){
            JsonObject object = new JsonObject();
            JsonArray workers = new JsonArray();
            for (WorkingPlayer workingPlayer : basicJob.getWorkingPlayers()){
                workers.add(workingPlayer.getJsonObject());
            }
            object.add(basicJob.getID(),workers);
            array.add(object);
        }
    }

    private void writeString(String write, File file) {
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(write.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
