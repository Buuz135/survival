package com.buuz135.survival.managers;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.foods.FoodPlant;
import com.buuz135.survival.foods.FoodRecipe;
import com.buuz135.survival.foods.FoodTree;
import com.buuz135.survival.utils.FoodUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;

public class FoodManager {

    public FoodManager() {
        InputStream file = SurvivalMain.getInstance().getClass().getResourceAsStream("/food.json");
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(file, writer, "UTF-8");
            String theString = writer.toString();
            JsonArray array = new JsonParser().parse(theString).getAsJsonArray();
            Iterator<JsonElement> iterator = array.iterator();
            while (iterator.hasNext()) {
                JsonObject object = iterator.next().getAsJsonObject();
                if (object.get("recipe").getAsJsonObject().get("type").getAsString().equals("PLANT")) {
                    new FoodPlant(object.get("displayName").getAsString(), object.get("texture").getAsString(),
                            object.get("food").getAsDouble(), object.get("saturation").getAsDouble(), object.get("UUID").getAsString(),
                            object.get("recipe").getAsJsonObject().get("minDrop").getAsInt(), object.get("recipe").getAsJsonObject().get("maxDrop").getAsInt());
                }
                if (object.get("recipe").getAsJsonObject().get("type").getAsString().equals("TREE")) {
                    new FoodTree(object.get("displayName").getAsString(), object.get("texture").getAsString(),
                            object.get("food").getAsDouble(), object.get("saturation").getAsDouble(), object.get("UUID").getAsString());
                }
                if (object.get("recipe").getAsJsonObject().get("type").getAsString().equals("CRAFT")) {
                    String[] shape = new String[3];
                    JsonArray shapeArray = object.getAsJsonObject("recipe").getAsJsonArray("shape");
                    int i = 0;
                    for (JsonElement element : shapeArray) {
                        shape[i] = element.getAsString();
                        ++i;
                    }
                    char[] chars = new char[9];
                    ItemStack[] stacks = new ItemStack[9];
                    i = 0;
                    for (Map.Entry<String, JsonElement> element : object.get("recipe").getAsJsonObject().getAsJsonObject("ingredients").entrySet()) {
                        chars[i] = element.getKey().charAt(0);
                        Map.Entry<String, JsonElement> data = element.getValue().getAsJsonObject().entrySet().iterator().next();
                        ItemStack item;
                        if (data.getKey().equals("FOOD"))
                            item = FoodUtils.getFoodFromUUID(data.getValue().getAsString()).getItem();
                        else
                            item = new ItemStack(Material.getMaterial(data.getKey()), 1, (short) data.getValue().getAsInt());
                        ++i;
                    }
                    new FoodRecipe(object.get("displayName").getAsString(), object.get("texture").getAsString(),
                            object.get("food").getAsDouble(), object.get("saturation").getAsDouble(), object.get("UUID").getAsString(),
                            object.get("recipe").getAsJsonObject().get("amount").getAsInt(), shape, chars, stacks);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
