package com.buuz135.survival.managers;


import com.buuz135.survival.portals.PlayerPortal;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;


public class RecipeManager {

    private Recipe portalItem;

    public RecipeManager() {
        ItemStack out = new ItemStack(Material.PAPER, 2);
        ItemMeta meta = out.getItemMeta();
        meta.setDisplayName(PlayerPortal.name);
        meta.setLore(Arrays.asList(ChatColor.BLUE + "" + ChatColor.BOLD + "Color: " + ChatColor.DARK_AQUA + "???", ChatColor.BLUE + "" + ChatColor.BOLD + "Id: " + ChatColor.DARK_AQUA + "???"));
        out.setItemMeta(meta);
        portalItem = new ShapedRecipe(out).shape("oeo", "ere", "oeo").setIngredient('o', Material.OBSIDIAN).setIngredient('e', Material.EYE_OF_ENDER).setIngredient('r', Material.REDSTONE_BLOCK);
        Bukkit.addRecipe(portalItem);
    }
}
