package com.buuz135.survival.managers;

import com.buuz135.survival.commands.CreateShopCommand;
import com.buuz135.survival.commands.FoodCommand;
import com.buuz135.survival.commands.GivePlotItemCommand;
import com.buuz135.survival.commands.MapRadarCommand;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_9_R2.CraftServer;
import org.bukkit.plugin.java.JavaPlugin;

public class CommandManager {

    private JavaPlugin instance;
    private CraftServer craftServer;

    public CommandManager(JavaPlugin instance) {
        this.instance = instance;
        this.craftServer = (CraftServer) Bukkit.getServer();
        registerCommands();
    }

    private void registerCommands() {
        craftServer.getCommandMap().register("/gpi", new GivePlotItemCommand("giveplotitem", instance));
        craftServer.getCommandMap().register("/mapa", new MapRadarCommand("mapa"));
        craftServer.getCommandMap().register("/tienda", new CreateShopCommand("tienda"));
        craftServer.getCommandMap().register("/food", new FoodCommand("food"));
    }
}
