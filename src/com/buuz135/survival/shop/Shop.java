package com.buuz135.survival.shop;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.utils.GUIUtils;
import com.buuz135.survival.utils.ItemStackBuilder;
import com.buuz135.survival.utils.SerializationUtils;
import com.google.gson.JsonObject;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_9_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_9_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Shop {

    public static List<Shop> shopList = new ArrayList<>();

    public static int IDGEN = 0;

    public static Shop getShop(Location sign) {
        for (Shop s : shopList) {
            if (s.getSign().equals(sign)) {
                return s;
            }
        }
        return null;
    }

    public static Shop getShopByBlock(Location sign) {
        for (Shop s : shopList) {
            if (s.getLocation().equals(sign)) {
                return s;
            }
        }
        return null;
    }

    public static Shop getShopByID(int id) {
        for (Shop s : shopList) {
            if (s.getId() == id) {
                return s;
            }
        }
        return null;
    }

    private UUID owner;
    private ItemStack itemStack;
    private int sell;
    private int buy;
    private boolean creative;
    private Location location;
    private Location sign;
    private EntityItem display;
    private int stored;
    private List<String> playersSee;
    private int id;


    public Shop(Player owner, ItemStack itemStack, int sell, int buy, boolean creative, Location location, Location sign) {
        this.owner = owner.getUniqueId();
        this.itemStack = itemStack;
        this.sell = sell;
        this.buy = buy;
        this.creative = creative;
        this.location = location;
        this.sign = sign;
        if (creative) {
            stored = Integer.MAX_VALUE;
        } else {
            stored = 0;
        }
        World w = ((CraftWorld) location.getWorld()).getHandle();
        display = new EntityItem(w, location.getBlockX() + 0.5, location.getBlockY() + 1, location.getBlockZ() + 0.5, new net.minecraft.server.v1_9_R2.ItemStack(Item.getById(itemStack.getTypeId()), itemStack.getAmount(), itemStack.getDurability()));
        display.setPosition(location.getBlockX() + 0.5, location.getBlockY() + 1, location.getBlockZ() + 0.5);
        playersSee = new ArrayList<>();
        shopList.add(this);
        this.id = ++IDGEN;
        construct();
    }

    public Shop(JsonObject jsonObject) {
        owner = UUID.fromString(jsonObject.get("owner").getAsString());
        sell = jsonObject.get("sell").getAsInt();
        buy = jsonObject.get("buy").getAsInt();
        itemStack = SerializationUtils.deserializeItemStack(jsonObject.get("itemStack").getAsJsonObject());
        creative = jsonObject.get("creative").getAsBoolean();
        location = SerializationUtils.deserializeLocation(jsonObject.get("location").getAsJsonObject());
        sign = SerializationUtils.deserializeLocation(jsonObject.get("sign").getAsJsonObject());
        if (creative) {
            stored = Integer.MAX_VALUE;
        } else {
            stored = jsonObject.get("stored").getAsInt();
        }
        World w = ((CraftWorld) location.getWorld()).getHandle();
        display = new EntityItem(w, location.getBlockX() + 0.5, location.getBlockY() + 1, location.getBlockZ() + 0.5, new net.minecraft.server.v1_9_R2.ItemStack(Item.getById(itemStack.getTypeId()), itemStack.getAmount(), itemStack.getDurability()));
        display.setPosition(location.getBlockX() + 0.5, location.getBlockY() + 1, location.getBlockZ() + 0.5);
        playersSee = new ArrayList<>();
        shopList.add(this);
        this.id = ++IDGEN;
        construct();
    }

    public String getOwnerName() {
        return Bukkit.getOfflinePlayer(owner).getName();
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public int getSell() {
        return sell;
    }

    public void setSell(int sell) {
        this.sell = sell;
    }

    public int getBuy() {
        return buy;
    }

    public void setBuy(int buy) {
        this.buy = buy;
    }

    public boolean isCreative() {
        return creative;
    }

    public void setCreative(boolean creative) {
        this.creative = creative;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getSign() {
        return sign;
    }

    public EntityItem getDisplay() {
        return display;
    }

    public int getId() {
        return id;
    }

    public void setSign(Location sign) {
        this.sign = sign;
    }

    public int getStored() {
        return stored;
    }

    public void setStored(int stored) {
        this.stored = stored;
    }

    public List<String> getPlayersSee() {
        return playersSee;
    }

    public JsonObject getJsonObject() {
        JsonObject object = new JsonObject();
        object.addProperty("owner", owner.toString());
        object.addProperty("sell", sell);
        object.addProperty("buy", buy);
        object.addProperty("stored", stored);
        object.add("itemStack", SerializationUtils.serializeItemStack(itemStack));
        object.addProperty("creative", creative);
        object.add("location", SerializationUtils.serializeLocation(location));
        object.add("sign", SerializationUtils.serializeLocation(sign));
        return object;
    }

    public Inventory getInventory() {
        Inventory inventory = Bukkit.createInventory(null, 9, "Tienda " + GUIUtils.hideInfo(id + ""));
        inventory.setItem(8, new ItemStackBuilder(Material.BARRIER).setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "Quitar Tienda").addLore(Arrays.asList(ChatColor.GRAY + "*Haz click para quitar la tienda*")).build());
        inventory.setItem(0, new ItemStackBuilder(Material.TORCH).setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + ChatColor.UNDERLINE + "Información").addLore(
                Arrays.asList(ChatColor.BLUE + "Cantidad: " + ChatColor.DARK_AQUA + stored, ChatColor.BLUE + "Compra: " + ChatColor.DARK_AQUA + buy, ChatColor.BLUE + "Venta: " + ChatColor.DARK_AQUA + sell)
        ).build());
        inventory.setItem(1, itemStack.clone());
        inventory.setItem(2, new ItemStackBuilder(Material.CHEST).setDisplayName(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Despósito").setLore(
                Arrays.asList(ChatColor.GRAY + "*Haz click izquierdo para depositar items*", ChatColor.GRAY + "*Haz click derecho para extraer items*")).build());
        return inventory;
    }

    public Inventory getExtractionInventory() {
        Inventory inventory = Bukkit.createInventory(null, 9, "Extracción " + GUIUtils.hideInfo(id + ""));
        Bukkit.getScheduler().runTaskAsynchronously(SurvivalMain.getInstance(), new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i < stored && i / itemStack.getMaxStackSize() <= 9) {
                    ++i;
                    ItemStack stack = itemStack.clone();
                    stack.setAmount(1);
                    inventory.addItem(stack);
                }
            }
        });
        return inventory;
    }

    public Inventory getTransactionInventory() {
        Inventory inventory = Bukkit.createInventory(null, 9 * 3, "Comprar/Vender " + GUIUtils.hideInfo(id + ""));
        int maxStack = itemStack.getMaxStackSize();
        inventory.setItem(10, new ItemStackBuilder(Material.EMERALD_BLOCK).setAmount(maxStack).setDisplayName(ChatColor.GREEN + "Comprar " + maxStack).setLore(Arrays.asList(ChatColor.GRAY + "*Haz click para comprar " + maxStack + " items*")).build());
        if (itemStack.getAmount() != maxStack)
            inventory.setItem(12, new ItemStackBuilder(Material.EMERALD).setAmount(itemStack.getAmount()).setDisplayName(ChatColor.GREEN + "Comprar " + itemStack.getAmount()).setLore(Arrays.asList(ChatColor.GRAY + "*Haz click para comprar " + itemStack.getAmount() + " items*")).build());
        if (itemStack.getAmount() != maxStack)
            inventory.setItem(14, new ItemStackBuilder(Material.REDSTONE).setAmount(itemStack.getAmount()).setDisplayName(ChatColor.RED + "Vender " + itemStack.getAmount()).setLore(Arrays.asList(ChatColor.GRAY + "*Haz click para vender " + itemStack.getAmount() + " items*")).build());
        inventory.setItem(16, new ItemStackBuilder(Material.REDSTONE_BLOCK).setAmount(maxStack).setDisplayName(ChatColor.GREEN + "Vender " + maxStack).setLore(Arrays.asList(ChatColor.GRAY + "*Haz click para vender " + maxStack + " items*")).build());
        return inventory;
    }

    public Inventory getDepositInventory() {
        return Bukkit.createInventory(null, 3 * 9, "Deposito " + GUIUtils.hideInfo(id + ""));
    }

    public void removeFakeItem() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(display.getId());
            EntityPlayer playerEnt = ((CraftPlayer) player).getHandle();
            playerEnt.playerConnection.sendPacket(destroy);
        }
    }

    private void construct() {
        Sign s = (Sign) sign.getBlock().getState();
        s.setLine(0, ChatColor.BLUE + "" + ChatColor.BOLD + "[QUARKFOUR]");
        s.setLine(1, ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Cant. " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + itemStack.getAmount());
        s.setLine(2, ChatColor.GREEN + "" + ChatColor.BOLD + "B " + ChatColor.GREEN + buy + ChatColor.BLACK + "" + ChatColor.BOLD + " | " + ChatColor.RED + sell + ChatColor.BOLD + " S");
        String lastLine = ChatColor.DARK_RED + "ADMIN";
        if (!creative) {
            lastLine = ChatColor.DARK_RED + getOwnerName();
        }
        s.setLine(3, lastLine);
        s.update();
    }

    public void deconstruct() {
        sign.getBlock().breakNaturally();
        removeFakeItem();
        shopList.remove(this);
    }
}
