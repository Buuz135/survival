package com.buuz135.survival.shop;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.utils.GUIUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopEventHandler implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.WALL_SIGN) {
            Shop s = Shop.getShop(event.getClickedBlock().getLocation());
            if (s != null) {
                if (event.getPlayer().getName().equals(s.getOwnerName())) {
                    event.getPlayer().openInventory(s.getInventory());
                } else {
                    event.getPlayer().openInventory(s.getTransactionInventory());
                }
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Inventory inv = event.getWhoClicked().getOpenInventory().getTopInventory();
        if (inv.getName().startsWith("Tienda") || inv.getName().startsWith("Extracción") || inv.getName().startsWith("Comprar/Vender"))
            event.setCancelled(true);
        if (event.getClickedInventory().getName().startsWith("Tienda")) {
            Shop s = Shop.getShopByID(Integer.parseInt(GUIUtils.getLoreHideInfo(event.getClickedInventory().getName().split(" ")[1])));
            if (s != null) {
                if (event.getSlot() == 2) {
                    if (event.isRightClick()) {
                        event.getWhoClicked().openInventory(s.getExtractionInventory());
                    } else {
                        event.getWhoClicked().openInventory(s.getDepositInventory());
                    }
                }
                if (event.getSlot() == 8) {
                    event.getWhoClicked().closeInventory();
                    s.deconstruct();
                }
            }

        }
        if (event.getClickedInventory().getName().startsWith("Extracción ")) {
            Shop s = Shop.getShopByID(Integer.parseInt(GUIUtils.getLoreHideInfo(event.getClickedInventory().getName().split(" ")[1])));
            if (s != null) {
                if (event.getClickedInventory().getItem(event.getSlot()) != null) {
                    int amount = event.getClickedInventory().getItem(event.getSlot()).getAmount() / 2;
                    if (event.isLeftClick()) {
                        amount = event.getClickedInventory().getItem(event.getSlot()).getAmount();
                    }
                    if (event.getClickedInventory().getItem(event.getSlot()).getAmount() == 1) amount = 1;
                    if (hasFreeSlot((Player) event.getWhoClicked())) {
                        ItemStack stack = s.getItemStack().clone();
                        stack.setAmount(amount);
                        s.setStored(s.getStored() - amount);
                        event.getWhoClicked().getInventory().addItem(stack);
                        event.getWhoClicked().openInventory(s.getExtractionInventory());
                    } else {
                        event.getWhoClicked().sendMessage(SurvivalMain.TAG + ChatColor.RED + "No tienes espacio suficiente en tu inventario.");
                    }
                }
            }
        }
        if (event.getClickedInventory().getName().startsWith("Comprar/Vender")) {
            Shop s = Shop.getShopByID(Integer.parseInt(GUIUtils.getLoreHideInfo(event.getClickedInventory().getName().split(" ")[1])));
            if (s != null) {
                ItemStack stack = event.getInventory().getItem(event.getSlot());
                if (stack != null) {
                    if (stack.getType() == Material.EMERALD_BLOCK) {
                        buy((Player) event.getWhoClicked(), stack.getMaxStackSize(), s);
                    }
                    if (stack.getType() == Material.EMERALD) {
                        buy((Player) event.getWhoClicked(), s.getItemStack().getAmount(), s);
                    }
                    if (stack.getType() == Material.REDSTONE) {
                        sell((Player) event.getWhoClicked(), s.getItemStack().getAmount(), s);
                    }
                    if (stack.getType() == Material.REDSTONE_BLOCK) {
                        sell((Player) event.getWhoClicked(), stack.getMaxStackSize(), s);
                    }
                }
            }
        }
    }


    @EventHandler
    public void inventoryClose(InventoryCloseEvent event) {
        if (event.getInventory().getName().startsWith("Deposito")) {
            Shop s = Shop.getShopByID(Integer.parseInt(GUIUtils.getLoreHideInfo(event.getInventory().getName().split(" ")[1])));
            if (s != null) {
                int amount = 0;
                for (ItemStack stack : event.getInventory()) {
                    if (stack != null) {
                        if (stack.isSimilar(s.getItemStack())) {
                            amount += stack.getAmount();
                        } else {
                            event.getPlayer().getInventory().addItem(stack);
                        }
                    }
                }
                s.setStored(s.getStored() + amount);
            }
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if (Shop.getShopByBlock(event.getBlock().getLocation()) != null || Shop.getShop(event.getBlock().getLocation()) != null)
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if (Shop.getShopByBlock(event.getBlock().getRelative(BlockFace.DOWN).getLocation()) != null)
            event.setCancelled(true);
    }

    private boolean hasFreeSlot(Player player) {
        for (ItemStack stack : player.getInventory()) {
            if (stack == null) return true;
        }
        return false;
    }

    private void sell(Player player, double amount, Shop shop) {
        double price = shop.getSell() * (amount / shop.getItemStack().getAmount());
        ItemStack check = shop.getItemStack().clone();
        check.setAmount((int) amount);
        if (player.getInventory().containsAtLeast(check, (int) amount)) {
            if (!shop.isCreative()) {
                if (SurvivalMain.getInstance().getEconomyManager().getEconomy().has(Bukkit.getOfflinePlayer(shop.getOwnerName()), price)) {
                    SurvivalMain.getInstance().getEconomyManager().getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(shop.getOwnerName()), price);
                } else {
                    player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "El dueño de la tienda no tiene suficiente dinero para realizar esta transacción.");
                    return;
                }
            }
            player.getInventory().removeItem(check);
            SurvivalMain.getInstance().getEconomyManager().getEconomy().depositPlayer(player, price);
            player.sendMessage(SurvivalMain.TAG + ChatColor.GREEN + "Se han añadido " + price + SurvivalMain.getInstance().getEconomyManager().getEconomy().currencyNameSingular() + " por la venta de items.");
        } else {
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "No tienes suficiente items para vender.");
        }
    }

    private void buy(Player player, double amount, Shop shop) {
        double price = shop.getSell() * (amount / shop.getItemStack().getAmount());
        if (SurvivalMain.getInstance().getEconomyManager().getEconomy().has(player, price)) {
            if (shop.getStored() >= amount) {
                if (hasFreeSlot(player)) {
                    SurvivalMain.getInstance().getEconomyManager().getEconomy().withdrawPlayer(player, amount);
                    if (!shop.isCreative())
                        SurvivalMain.getInstance().getEconomyManager().getEconomy().depositPlayer(Bukkit.getOfflinePlayer(shop.getOwnerName()), amount);
                    ItemStack stack = shop.getItemStack().clone();
                    stack.setAmount((int) amount);
                    player.getInventory().addItem(stack);
                    player.sendMessage(SurvivalMain.TAG + ChatColor.GREEN + "Se han retirado " + price + SurvivalMain.getInstance().getEconomyManager().getEconomy().currencyNameSingular() + " de tu cuenta.");
                } else {
                    player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "No se ha podido encontrar espacio en tu inventario.");
                }
            } else {
                player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "La tienda no tiene suficiente items para vender.");
            }
        } else {
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "No tienes suficiente dinero para completar la transacción.");
        }
    }
}
