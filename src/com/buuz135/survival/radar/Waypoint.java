package com.buuz135.survival.radar;

import com.buuz135.survival.utils.SerializationUtils;
import com.google.gson.JsonObject;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;

public class Waypoint {

    private ChatColor color;
    private Location location;
    private String name;
    private boolean visible;

    public Waypoint(ChatColor color, Location location, String name) {
        this.color = color;
        this.location = location;
        this.name = name;
        this.visible = true;
    }

    public Waypoint(JsonObject jsonObject) {
        this.color = ChatColor.valueOf(jsonObject.get("color").getAsString());
        this.location = SerializationUtils.deserializeLocation(jsonObject.get("location").getAsJsonObject());
        this.name = jsonObject.get("name").getAsString();
        this.visible = jsonObject.get("visible").getAsBoolean();
    }

    public ChatColor getColor() {
        return color;
    }

    public void setColor(ChatColor color) {
        this.color = color;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("color", color.name());
        jsonObject.add("location", SerializationUtils.serializeLocation(location));
        jsonObject.addProperty("visible", visible);
        return jsonObject;
    }
}
