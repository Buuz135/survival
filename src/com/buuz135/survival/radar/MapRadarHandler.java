package com.buuz135.survival.radar;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class MapRadarHandler implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        MapRadar.mapRadars.stream().filter(map -> map.getPlayer().equals(event.getPlayer().getUniqueId()) && map.isVisible()).forEach(map -> map.getBar().addPlayer(event.getPlayer()));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        MapRadar.mapRadars.stream().filter(map -> map.getPlayer().equals(event.getPlayer().getUniqueId()) && map.isVisible()).forEach(map -> map.getBar().removePlayer(event.getPlayer()));
    }
}
