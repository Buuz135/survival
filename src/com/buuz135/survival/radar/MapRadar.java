package com.buuz135.survival.radar;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MapRadar {

    public static List<MapRadar> mapRadars = new ArrayList<>();

    public static MapRadar getRadar(Player player) {
        for (MapRadar mapRadar : mapRadars) {
            if (mapRadar.getPlayer().equals(player.getUniqueId())) {
                return mapRadar;
            }
        }
        return new MapRadar(player);
    }

    private UUID player;
    private List<Waypoint> locationList;
    private BossBar bar;
    private boolean visible;


    public MapRadar(Player player) {
        this.player = player.getUniqueId();
        this.locationList = new ArrayList<>();
        this.bar = Bukkit.createBossBar(getName(), BarColor.BLUE, BarStyle.SOLID);
        this.visible = true;
        bar.addPlayer(player);
        mapRadars.add(this);
    }

    public MapRadar(JsonObject jsonObject) {
        this.player = UUID.fromString(jsonObject.get("owner").getAsString());
        this.locationList = new ArrayList<>();
        this.bar = Bukkit.createBossBar(getName(), BarColor.valueOf(jsonObject.get("bar").getAsJsonObject().get("color").getAsString()), BarStyle.valueOf(jsonObject.get("bar").getAsJsonObject().get("style").getAsString()));
        this.visible = jsonObject.get("visible").getAsBoolean();
        JsonArray array = jsonObject.getAsJsonArray("waypoints");
        for (JsonElement element : array) {
            this.locationList.add(new Waypoint(element.getAsJsonObject()));
        }
        mapRadars.add(this);
    }

    public String getName() {
        Player player = Bukkit.getPlayer(this.getPlayer());
        String[] name = new String[60];
        for (int i = 0; i < name.length; ++i) {
            name[i] = " ";
        }
        Vector playerDir = player.getLocation().getDirection();
        playerDir.setY(0);
        for (Waypoint waypoint : locationList) {
            if (waypoint.isVisible()) {
                Vector position = waypoint.getLocation().clone().subtract(player.getLocation()).toVector().normalize();
                position.setY(0);
                if (isVisible(position.angle(playerDir))) {
                    char c = 'x';
                    double angle = position.angle(playerDir);
                    boolean right = position.getX() * -playerDir.getZ() + position.getZ() * playerDir.getX() > 0;
                    if (waypoint.getLocation().getY() > player.getLocation().getY() + 32) c = '^';
                    if (waypoint.getLocation().getY() + 32 < player.getLocation().getY()) c = 'v';
                    int pos = (int) ((name.length / 2) * angle);
                    if (!right) {
                        pos = -pos;
                    }
                    pos = name.length / 2 + pos;
                    if (pos > 0 && pos < name.length) {
                        name[pos] = waypoint.getColor() + "" + c;
                    }
                }
            }
        }
        String finalname = "";
        for (String s : name) {
            finalname += s;
        }
        return finalname;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public BossBar getBar() {
        return bar;
    }

    public void setBar(BossBar bar) {
        this.bar = bar;
    }

    private boolean isVisible(float angle) {
        return angle > 0 && angle < 1;
    }

    public UUID getPlayer() {
        return player;
    }

    public List<Waypoint> getLocationList() {
        return locationList;
    }

    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("owner", player.toString());
        JsonArray array = new JsonArray();
        for (Waypoint waypoint : locationList) array.add(waypoint.getJsonObject());
        jsonObject.add("waypoints", array);
        JsonObject bar = new JsonObject();
        bar.addProperty("color", this.bar.getColor().toString());
        bar.addProperty("style", this.bar.getStyle().toString());
        jsonObject.add("bar", bar);
        jsonObject.addProperty("visible", visible);
        return jsonObject;
    }
}
