package com.buuz135.survival.flag;

import com.buuz135.survival.protectedarea.SurvivalPlot;
import com.buuz135.survival.utils.LocationUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.List;

public class FlagEventHandler implements Listener {

    @EventHandler
    public void tntAndCreeperFlagDetection(EntityExplodeEvent event) {
        if (event.getEntity() instanceof TNTPrimed) {
            List<Block> protBlocks = new ArrayList<>();
            for (Block block : event.blockList()) {
                SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(block.getLocation());
                if (plot != null) {
                    if (!plot.getFlaresExternal().contains(Flag.TNT) || !plot.getFlaresMembers().contains(Flag.TNT)) {
                        protBlocks.add(block);
                    }
                }
            }
            protBlocks.forEach(block -> event.blockList().remove(block));
        }
        if (event.getEntity() instanceof Creeper) {
            List<Block> protBlocks = new ArrayList<>();
            for (Block block : event.blockList()) {
                SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(block.getLocation());
                if (plot != null) {
                    if (!plot.getFlaresExternal().contains(Flag.CREEPER) || !plot.getFlaresMembers().contains(Flag.CREEPER)) {
                        protBlocks.add(block);
                    }
                }
            }
            protBlocks.forEach(block -> event.blockList().remove(block));
        }
    }

    @EventHandler
    public void spawnMobsFlagDetection(CreatureSpawnEvent event) {
        SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getEntity().getLocation());
        if (plot != null) {
            event.setCancelled(!plot.getFlaresExternal().contains(Flag.MONSTER) || !plot.getFlaresMembers().contains(Flag.MONSTER));
        }
    }

    @EventHandler
    public void waterAndLavaFlagDetection(BlockFromToEvent event) {
        SurvivalPlot plotTo = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getToBlock().getLocation());
        SurvivalPlot plotFrom = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getBlock().getLocation());
        if (plotTo != null) { // ESTA DENTRO DEL PLOT LO QUE SE EXPANDE
            if (plotFrom != null) {// VIENE DE NUESTRO PLOT O DE OTRO
                if (plotTo.equals(plotFrom)) { //VIENE DE NUESTRO
                    if (event.getBlock().getType() == Material.STATIONARY_WATER) {
                        event.setCancelled(!plotTo.getFlaresMembers().contains(Flag.WATER));
                    }
                    if (event.getBlock().getType() == Material.STATIONARY_LAVA) {
                        event.setCancelled(!plotTo.getFlaresMembers().contains(Flag.LAVA));
                    }
                } else { //VIENE DE OTRO
                    if (event.getBlock().getType() == Material.STATIONARY_WATER) {
                        event.setCancelled(!plotTo.getFlaresExternal().contains(Flag.WATER));
                    }
                    if (event.getBlock().getType() == Material.STATIONARY_LAVA) {
                        event.setCancelled(!plotTo.getFlaresExternal().contains(Flag.LAVA));
                    }
                }
            } else { //VIENE DE NADA
                if (event.getBlock().getType() == Material.STATIONARY_WATER) {
                    event.setCancelled(!plotTo.getFlaresExternal().contains(Flag.WATER));
                }
                if (event.getBlock().getType() == Material.STATIONARY_LAVA) {
                    event.setCancelled(!plotTo.getFlaresExternal().contains(Flag.LAVA));
                }
            }
        }
    }

    @EventHandler
    public void bucketFlagDetection(PlayerInteractEvent event) {
        if (event.hasItem() && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (event.getItem().getType() == Material.WATER_BUCKET || event.getItem().getType() == Material.LAVA_BUCKET) {
                SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getClickedBlock().getLocation());
                if (plot != null) {
                    if (event.getItem().getType() == Material.WATER_BUCKET) {
                        if (plot.getMembers().contains(event.getPlayer().getUniqueId())) {
                            event.setCancelled(!plot.getFlaresMembers().contains(Flag.WATER));
                        } else {
                            event.setCancelled(!plot.getFlaresExternal().contains(Flag.WATER));
                        }
                    }
                    if (event.getItem().getType() == Material.LAVA_BUCKET) {
                        if (plot.getMembers().contains(event.getPlayer().getUniqueId())) {
                            event.setCancelled(!plot.getFlaresMembers().contains(Flag.LAVA));
                        } else {
                            event.setCancelled(!plot.getFlaresExternal().contains(Flag.LAVA));
                        }
                    }
                }
            }
        }
        if (event.getClickedBlock() != null && !event.isBlockInHand()) {
            SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getClickedBlock().getLocation());
            if (plot != null) {
                if (plot.getMembers().contains(event.getPlayer().getUniqueId())) {
                    event.setCancelled(!plot.getFlaresMembers().contains(Flag.INTERACT));
                } else if (!plot.getOwner().equals(event.getPlayer().getUniqueId())) {
                    event.setCancelled(!plot.getFlaresExternal().contains(Flag.INTERACT));
                }
            }
        }
    }

    @EventHandler
    public void interactInv(InventoryOpenEvent event) {
        SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getPlayer().getLocation());
        if (plot != null) {
            if (plot.getMembers().contains(event.getPlayer().getUniqueId())) {
                event.setCancelled(!plot.getFlaresMembers().contains(Flag.INTERACT));
            } else if (!plot.getOwner().equals(event.getPlayer().getUniqueId())) {
                event.setCancelled(!plot.getFlaresExternal().contains(Flag.INTERACT));
            }
        }
    }

    @EventHandler
    public void blockPlaceFlag(BlockPlaceEvent event) {
        SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getBlock().getLocation());
        if (plot != null) {
            if (plot.getMembers().contains(event.getPlayer().getUniqueId())) {
                event.setCancelled(!plot.getFlaresMembers().contains(Flag.PLACE));
            } else if (!plot.getOwner().equals(event.getPlayer().getUniqueId())) {
                event.setCancelled(!plot.getFlaresExternal().contains(Flag.PLACE));
            }
        }
    }

    @EventHandler
    public void enterFlagEvent(PlayerMoveEvent event) {
        SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getTo());
        if (plot != null && !plot.getOwner().equals(event.getPlayer().getUniqueId()) && !plot.getMembers().contains(event.getPlayer().getUniqueId())) {
            if (!plot.getFlaresExternal().contains(Flag.ENTER) || !plot.getFlaresMembers().contains(Flag.ENTER)) {

                if (plot.equals(SurvivalPlot.getSurvivalPlotFromLocationArea(event.getFrom()))) {
                    event.getPlayer().teleport(LocationUtils.getRandomLocationFromOutside(plot.getCenter(), plot.getSize(), 0));
                } else event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void commandFlagEvent(PlayerCommandPreprocessEvent event) {
        SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getPlayer().getLocation());
        if (plot != null) {
            if (plot.getMembers().contains(event.getPlayer().getUniqueId())) {
                event.setCancelled(!plot.getFlaresMembers().contains(Flag.COMANDS));
            } else if (!plot.getOwner().equals(event.getPlayer().getUniqueId())) {
                event.setCancelled(!plot.getFlaresExternal().contains(Flag.COMANDS));
            }
        }
    }

    @EventHandler
    public void onPVP(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocationArea(event.getEntity().getLocation());
            if (plot != null) {
                if (plot.isMember(event.getDamager().getUniqueId()) && plot.isMember(event.getEntity().getUniqueId())) {
                    event.setCancelled(!plot.getFlaresMembers().contains(Flag.PVP));
                } else {
                    event.setCancelled(!plot.getFlaresExternal().contains(Flag.PVP));
                }
            }
        }
    }


}
