package com.buuz135.survival.flag;

public enum Flag {
    TNT("Explosión de TNT", Type.GENERIC),
    CREEPER("Explosión de creepers", Type.GENERIC),
    MONSTER("Spawneo de monstruos", Type.GENERIC),
    LAVA("Poner lava", Type.EXCLUSIVE),
    WATER("Poner agua", Type.EXCLUSIVE),
    INTERACT("Interactuar con bloques", Type.EXCLUSIVE),
    PLACE("Poner bloques", Type.EXCLUSIVE),
    ENTER("Entrar en el area", Type.GENERIC),
    COMANDS("Usar comandos", Type.EXCLUSIVE),
    PVP("PVP", Type.EXCLUSIVE);
    private String name;

    private Type type;

    private Flag(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        GENERIC, EXCLUSIVE
    }
}
