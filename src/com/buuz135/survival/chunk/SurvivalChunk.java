package com.buuz135.survival.chunk;

import com.buuz135.survival.utils.SerializationUtils;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SurvivalChunk {

    private int x;
    private int z;
    private File file;

    public SurvivalChunk(int x, int z, File folder) {
        this.x = x;
        this.z = z;
        this.file = new File(folder.getPath() + File.separator + x + "#" + z + ".yml");
        if (!file.exists()) try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    public void addLocation(Location location) throws IOException, InvalidConfigurationException {
        FileConfiguration fileConfiguration = new YamlConfiguration();
        fileConfiguration.load(file);
        List<String> locations = fileConfiguration.getStringList("locations");
        if (locations == null) locations = new ArrayList<>();
        locations.add(SerializationUtils.serializeLoc(location));
        fileConfiguration.set("locations", locations);
        try {
            fileConfiguration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean exists(Location location) throws IOException, InvalidConfigurationException {
        FileConfiguration fileConfiguration = new YamlConfiguration();
        fileConfiguration.load(file);
        List<String> locations = fileConfiguration.getStringList("locations");
        if (locations == null) locations = new ArrayList<>();
        return locations.contains(SerializationUtils.serializeLoc(location));
    }

    public void removeLocation(Location location) throws IOException, InvalidConfigurationException {
        FileConfiguration fileConfiguration = new YamlConfiguration();
        fileConfiguration.load(file);
        List<String> locations = fileConfiguration.getStringList("locations");
        if (locations == null) locations = new ArrayList<>();
        locations.remove(SerializationUtils.serializeLoc(location));
        fileConfiguration.set("locations", locations);
        try {
            fileConfiguration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
