package com.buuz135.survival.chunk;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.managers.ChunkManager;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.io.IOException;
import java.util.List;

public class SurvivalChunkHandler implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlace(BlockPlaceEvent event) {
        if (!SurvivalMain.getInstance().getChunkManager().getMaterials().contains(event.getBlockPlaced().getType()))
            return;
        SurvivalChunk chunk = SurvivalMain.getInstance().getChunkManager().getChunk(event.getBlockPlaced().getState().getLocation());
        try {
            chunk.addLocation(event.getBlock().getLocation());
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBreak(BlockBreakEvent event) {
        if (!SurvivalMain.getInstance().getChunkManager().getMaterials().contains(event.getBlock().getType())) return;
        SurvivalChunk chunk = SurvivalMain.getInstance().getChunkManager().getChunk(event.getBlock().getState().getLocation());
        try {
            if (chunk.exists(event.getBlock().getLocation())) {
                chunk.removeLocation(event.getBlock().getLocation());
            }
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onExtendPiston(BlockPistonExtendEvent event) throws IOException, InvalidConfigurationException {
        move(event.getBlocks(), event.getDirection());
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onRetract(BlockPistonRetractEvent event) throws IOException, InvalidConfigurationException {
        move(event.getBlocks(), event.getDirection());
    }

    private void move(List<Block> blocks, BlockFace direction) throws IOException, InvalidConfigurationException {
        ChunkManager manager = SurvivalMain.getInstance().getChunkManager();
        for (Block block : blocks) {
            if (SurvivalMain.getInstance().getChunkManager().getMaterials().contains(block.getType())) {
                Location actual = block.getLocation();
                SurvivalChunk actualChunk = manager.getChunk(actual);
                if (actualChunk.exists(actual)) {
                    actualChunk.removeLocation(actual);
                    Location after = block.getRelative(direction).getLocation();
                    SurvivalChunk afterChunk = manager.getChunk(after);
                    afterChunk.addLocation(after);
                }
            }
        }
    }
}
