package com.buuz135.survival.protectedarea;


import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.commands.GivePlotItemCommand;
import com.buuz135.survival.flag.Flag;
import com.buuz135.survival.utils.GUIUtils;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_9_R2.PacketPlayOutWorldBorder;
import net.minecraft.server.v1_9_R2.WorldBorder;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;
import java.util.List;

public class SurvivalPlotHandler implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getItemInHand() != null && event.getItemInHand().getType() == Material.BANNER && event.getItemInHand().hasItemMeta() && event.getItemInHand().getItemMeta().getDisplayName().equals(GivePlotItemCommand.NAME)) {
            Location current = event.getBlock().getLocation().clone();
            current.setY(0);
            for (SurvivalPlot plot : SurvivalPlot.survivalPlots) {
                Location to = plot.getCenter().clone();
                to.setY(0);
                if (to.distance(current) <= SurvivalPlot.MAX_AREA + SurvivalPlot.MIN_DIST) {
                    event.getPlayer().sendMessage(SurvivalMain.TAG + ChatColor.RED + "La protección esta muy cerca de otra, por favor busca otro lugar.");
                    event.setCancelled(true);
                    return;
                }
            }
            new SurvivalPlot(event.getPlayer(), event.getBlock().getLocation(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), SurvivalPlot.MIN_AREA);
            event.getPlayer().sendMessage(SurvivalMain.TAG + ChatColor.GREEN + "La parcela se ha creado satisfactoriamente.");
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_ANVIL_PLACE, 1, 1);
            for (int x = -SurvivalPlot.MIN_AREA / 2; x <= SurvivalPlot.MIN_AREA / 2; ++x) {
                for (int z = -SurvivalPlot.MIN_AREA / 2; z <= SurvivalPlot.MIN_AREA / 2; ++z) {
                    Location l = event.getBlockPlaced().getLocation().clone().add(x, 0.1, z);
                    event.getPlayer().playEffect(l, Effect.HAPPY_VILLAGER, 10);
                }
            }
        }

    }


    @EventHandler
    public void interactMainBlock(PlayerInteractEvent event) {
        if (event.getClickedBlock() != null && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocation(event.getClickedBlock().getLocation());
            if (plot != null) {
                event.getPlayer().openInventory(plot.createSurvivalPlotInventory());
                event.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromLocation(event.getBlock().getLocation());
        if (plot != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void inventoryClick(InventoryClickEvent event) {
        if (event.getClickedInventory().getName().startsWith("Menú")) {
            event.setCancelled(true);
            SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromID(Integer.parseInt(GUIUtils.getLoreHideInfo(event.getClickedInventory().getName().split(" ")[1])));
            if (plot != null) {
                if (event.getSlot() == 1) {
                    event.getWhoClicked().openInventory(plot.createFlaresMembersInventory());
                }
                if (event.getSlot() == 2) {
                    event.getWhoClicked().openInventory(plot.createFlaresExternalInventory());
                }
                if (event.getSlot() == 6) {
                    if (plot.getSize() + SurvivalPlot.INCREMENT <= SurvivalPlot.MAX_AREA) {
                        if (SurvivalMain.getInstance().getEconomyManager().getEconomy().has(Bukkit.getPlayer(plot.getOwner()), SurvivalPlot.PRICE)) {
                            plot.setSize(plot.getSize() + SurvivalPlot.INCREMENT);
                            ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                            SurvivalMain.getInstance().getEconomyManager().getEconomy().withdrawPlayer(Bukkit.getPlayer(plot.getOwner()), SurvivalPlot.PRICE);
                        } else {
                            event.getWhoClicked().sendMessage(SurvivalMain.TAG + ChatColor.RED + "No tienes suficiente dinero para hacer eso.");
                        }
                    } else {
                        event.getWhoClicked().sendMessage(SurvivalMain.TAG + ChatColor.RED + "Has llegado al tamaño máximo.");
                    }
                }
                if (event.getSlot() == 7) {
                    if (event.isLeftClick()) {
                        int size = plot.getSize();
                        if (size % 2 == 0) {
                            ++size;
                        }
                        sendBorder((Player) event.getWhoClicked(), size, plot.getCenter().getBlockX() + 0.5, plot.getCenter().getBlockZ() + 0.5);
                    } else {
                        removeBorder((Player) event.getWhoClicked());
                    }
                }
                if (event.getSlot() == 8) {
                    plot.getCenter().getBlock().setType(Material.AIR);
                    GivePlotItemCommand.giveBanner((Player) event.getWhoClicked());
                    ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ENTITY_ARMORSTAND_BREAK, 1, 1);
                    removeBorder((Player) event.getWhoClicked());
                    event.getWhoClicked().closeInventory();
                    SurvivalPlot.survivalPlots.remove(plot);
                }
            }
        }
        if (event.getClickedInventory().getName().startsWith("Opciones de miembros")) {
            event.setCancelled(true);
            if (event.getClickedInventory().getItem(event.getSlot()) != null) {
                SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromID(Integer.parseInt(GUIUtils.getLoreHideInfo(event.getClickedInventory().getName().split(" ")[3])));
                Flag flag = Flag.values()[event.getSlot()];
                if (plot.getFlaresMembers().contains(flag)) {
                    plot.getFlaresMembers().remove(flag);
                    ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ENTITY_ITEMFRAME_REMOVE_ITEM, 1, 1);
                } else {
                    plot.getFlaresMembers().add(flag);
                    ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ENTITY_ITEMFRAME_ADD_ITEM, 1, 1);
                }
                event.getWhoClicked().openInventory(plot.createFlaresMembersInventory());
            }
        }
        if (event.getClickedInventory().getName().startsWith("Opciones externas")) {
            event.setCancelled(true);
            if (event.getClickedInventory().getItem(event.getSlot()) != null) {
                SurvivalPlot plot = SurvivalPlot.getSurvivalPlotFromID(Integer.parseInt(GUIUtils.getLoreHideInfo(event.getClickedInventory().getName().split(" ")[2])));
                Flag flag = Flag.values()[event.getSlot()];
                if (plot.getFlaresExternal().contains(flag)) {
                    plot.getFlaresExternal().remove(flag);
                    ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ENTITY_ITEMFRAME_REMOVE_ITEM, 1, 1);
                } else {
                    plot.getFlaresExternal().add(flag);
                    ((Player) event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ENTITY_ITEMFRAME_ADD_ITEM, 1, 1);
                }
                event.getWhoClicked().openInventory(plot.createFlaresExternalInventory());
            }
        }
    }

    public static List<String> playersWithBorder = new ArrayList<>();

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.getFrom().getBlock().getX() != event.getTo().getBlock().getX() || event.getFrom().getBlock().getZ() != event.getTo().getBlock().getZ()) {
            if (event.getPlayer().getItemInHand().getType() == Material.BANNER && event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(GivePlotItemCommand.NAME)) {
                sendBorder(event.getPlayer(), SurvivalPlot.MIN_AREA, event.getTo().getBlockX() + 0.5, event.getTo().getBlockZ() + 0.5);
                playersWithBorder.add(event.getPlayer().getName());
            } else if (playersWithBorder.contains(event.getPlayer().getName())) {
                playersWithBorder.remove(event.getPlayer().getName());
                removeBorder(event.getPlayer());
            }
        }
    }

    private void sendBorder(Player player, int size, double x, double z) {
        WorldBorder border = new WorldBorder();
        border.setCenter(x, z);
        border.setSize(size);
        PacketPlayOutWorldBorder bordeR2 = new PacketPlayOutWorldBorder(border, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE);
        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(bordeR2);
    }

    private void removeBorder(Player player) {
        WorldBorder border = new WorldBorder();
        border.setCenter(0, 0);
        border.setSize(Integer.MAX_VALUE);
        PacketPlayOutWorldBorder bordeR2 = new PacketPlayOutWorldBorder(border, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE);
        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(bordeR2);
    }
}
