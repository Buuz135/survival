package com.buuz135.survival.protectedarea;

import com.buuz135.survival.flag.Flag;
import com.buuz135.survival.utils.GUIUtils;
import com.buuz135.survival.utils.ItemStackBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class SurvivalPlot {

    public static List<SurvivalPlot> survivalPlots = new ArrayList<>();

    public static final int MAX_AREA = 30;
    public static final int MIN_DIST = 32;
    public static final int MIN_AREA = 10;
    public static final int PRICE = 10000;
    public static final int INCREMENT = 10;

    public static int IDGEN = 0;

    public static SurvivalPlot getSurvivalPlotFromLocation(Location location) {
        for (SurvivalPlot s : survivalPlots) {
            if (location.equals(s.getCenter())) return s;
        }
        return null;
    }

    public static SurvivalPlot getSurvivalPlotFromLocationArea(Location location) {
        for (SurvivalPlot s : survivalPlots) {
            if (s.isInside(location)) return s;
        }
        return null;
    }

    public static SurvivalPlot getSurvivalPlotFromID(int id) {
        for (SurvivalPlot s : survivalPlots) {
            if (s.getId() == id) return s;
        }
        return null;
    }

    private UUID owner;
    private List<UUID> members;
    private List<Flag> flaresMembers;
    private List<Flag> flaresExternal;
    private Location center;
    private int size;
    private int id;

    public SurvivalPlot(Player owner, Location center, List<Flag> flaresMembers, List<Flag> flaresExternal, List<UUID> members, int size) {
        this.owner = owner.getUniqueId();
        this.center = center;
        this.flaresMembers = flaresMembers;
        this.members = members;
        this.size = size;
        this.flaresExternal = flaresExternal;
        this.id = ++IDGEN;
        survivalPlots.add(this);
    }

    public SurvivalPlot(JsonObject object) {
        owner = UUID.fromString(object.get("owner").getAsString());
        String[] l = object.get("locationCenter").getAsString().split(":");
        center = new Location(Bukkit.getWorld(l[0]), Integer.parseInt(l[1]), Integer.parseInt(l[2]), Integer.parseInt(l[3]));
        members = new ArrayList<>();
        object.get("members").getAsJsonArray().forEach(jsonElement -> members.add(UUID.fromString(jsonElement.getAsString())));
        flaresMembers = new ArrayList<>();
        object.get("flares").getAsJsonObject().get("members").getAsJsonArray().forEach(jsonElement -> flaresMembers.add(Flag.valueOf(jsonElement.getAsString())));
        flaresExternal = new ArrayList<>();
        object.get("flares").getAsJsonObject().get("external").getAsJsonArray().forEach(jsonElement -> flaresExternal.add(Flag.valueOf(jsonElement.getAsString())));
        size = object.get("size").getAsInt();
        this.id = ++IDGEN;
        survivalPlots.add(this);
    }

    public Location getCenter() {
        return center;
    }

    public Inventory createSurvivalPlotInventory() {
        Inventory inventory = Bukkit.createInventory(null, 9, "Menú " + GUIUtils.hideInfo(id + ""));
        ItemStack info = new ItemStack(Material.TORCH);
        ItemMeta meta = info.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Información");
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.BLUE + "Dueño: ");
        lore.add(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + Bukkit.getOfflinePlayer(owner).getName());
        lore.add(ChatColor.BLUE + "Miembros: ");
        if (members.size() == 0) {
            lore.add(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + "Nadie");
        } else {
            for (UUID name : members)
                lore.add(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + Bukkit.getOfflinePlayer(name).getName());
        }
        meta.setLore(lore);
        info.setItemMeta(meta);
        inventory.setItem(0, info);
        ItemStack disband = new ItemStack(Material.BARRIER);
        meta = disband.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Quitar parcela");
        meta.setLore(Arrays.asList(ChatColor.GRAY + "*Haz click para eliminar la parcela*"));
        disband.setItemMeta(meta);
        inventory.setItem(8, disband);
        ItemStack flares = new ItemStack(Material.BANNER);
        BannerMeta bmeta = (BannerMeta) flares.getItemMeta();
        bmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Opciones de miembros");
        lore = new ArrayList<>();
        if (this.flaresMembers.size() == 0) {
            lore.add(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + "Ninguna");
        } else {
            for (Flag name : this.flaresMembers) lore.add(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + name.getName());
        }
        lore.add(ChatColor.GRAY + "*Haz click para editar*");
        bmeta.setLore(lore);
        flares.setItemMeta(bmeta);
        inventory.setItem(1, flares);
        ItemStack flaresExt = new ItemStack(Material.BANNER);
        meta = flaresExt.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Opciones externas");
        lore = new ArrayList<>();
        if (this.flaresExternal.size() == 0) {
            lore.add(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + "Ninguna");
        } else {
            for (Flag name : this.flaresExternal)
                lore.add(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + name.getName());
        }
        lore.add(ChatColor.GRAY + "*Haz click para editar*");
        meta.setLore(lore);
        flaresExt.setItemMeta(meta);
        inventory.setItem(2, flaresExt);
        ItemStack area = new ItemStack(Material.IRON_FENCE);
        meta = area.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Area");
        meta.setLore(Arrays.asList(ChatColor.GRAY + "- " + ChatColor.DARK_AQUA + size + "x" + size, ChatColor.GRAY + "*Haz click izquierdo para mostrar el area*", ChatColor.GRAY + "*Haz click derecho para quitar el area*"));
        area.setItemMeta(meta);
        inventory.setItem(7, area);
        ItemStack upgrade = new ItemStackBuilder(Material.EXP_BOTTLE).setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Incrementar area")
                .setLore(Arrays.asList(ChatColor.BLUE + "Tamaño extra: " + ChatColor.DARK_AQUA + INCREMENT, ChatColor.BLUE + "Precio: " + ChatColor.DARK_AQUA + PRICE)).build();
        inventory.setItem(6, upgrade);
        return inventory;
    }

    public JsonObject getJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("owner", owner.toString());
        jsonObject.addProperty("locationCenter", createStringLocation());
        JsonArray players = new JsonArray();
        for (UUID name : members) {
            players.add(new JsonPrimitive(name.toString()));
        }
        jsonObject.add("members", players);
        JsonArray flares = new JsonArray();
        for (Flag flag : this.flaresMembers) {
            flares.add(new JsonPrimitive(flag.toString()));
        }
        JsonObject members = new JsonObject();
        members.add("members", flares);
        flares = new JsonArray();
        for (Flag flag : this.flaresExternal) {
            flares.add(new JsonPrimitive(flag.toString()));
        }
        members.add("external", flares);
        jsonObject.add("flares", members);
        jsonObject.addProperty("size", size);
        return jsonObject;
    }

    private String createStringLocation() {
        return center.getWorld().getName() + ":" + center.getBlockX() + ":" + center.getBlockY() + ":" + center.getBlockZ();
    }


    public int getSize() {
        return size;
    }

    public Inventory createFlaresMembersInventory() {
        Inventory inventory = Bukkit.createInventory(null, (Flag.values().length / 9 + 1) * 9, "Opciones de miembros " + GUIUtils.hideInfo("" + id));
        fillInventoryWithFlares(inventory, flaresMembers, flaresExternal);
        return inventory;
    }

    public Inventory createFlaresExternalInventory() {
        Inventory inventory = Bukkit.createInventory(null, (Flag.values().length / 9 + 1) * 9, "Opciones externas " + GUIUtils.hideInfo("" + id));
        fillInventoryWithFlares(inventory, flaresExternal, flaresMembers);
        return inventory;
    }

    private void fillInventoryWithFlares(Inventory inventory, List<Flag> flags, List<Flag> other) {
        for (Flag f : Flag.values()) {
            ItemStack stack = new ItemStack(Material.BANNER);
            BannerMeta meta = (BannerMeta) stack.getItemMeta();
            boolean contains = flags.contains(f);
            if (contains) {
                meta.setBaseColor(DyeColor.LIME);
            } else meta.setBaseColor(DyeColor.RED);
            meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + f.getName());
            List<String> lore = new ArrayList<>();
            lore.add(" ");
            if (f.getType() == Flag.Type.GENERIC) {
                lore.add(ChatColor.GOLD + "Tipo: " + ChatColor.YELLOW + "Genérico");
                if (!contains || !other.contains(f)) {
                    lore.add(ChatColor.GOLD + "Estado: " + ChatColor.YELLOW + "Desactivado");
                } else {
                    lore.add(ChatColor.GOLD + "Estado: " + ChatColor.YELLOW + "Activado");
                }
                if (contains != other.contains(f)) {
                    meta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.HALF_HORIZONTAL_MIRROR));
                }
            } else {
                lore.add(ChatColor.GOLD + "Tipo: " + ChatColor.YELLOW + "Exclusivo");
                if (contains) {
                    lore.add(ChatColor.GOLD + "Estado: " + ChatColor.YELLOW + "Activado");
                } else {
                    lore.add(ChatColor.GOLD + "Estado: " + ChatColor.YELLOW + "Desactivado");
                }
            }
            lore.add(ChatColor.GRAY + "*Haz click para cambiar*");
            meta.setLore(lore);
            meta.addItemFlags(ItemFlag.values());
            stack.setItemMeta(meta);
            inventory.addItem(stack);
        }
    }

    public boolean isInside(Location location) {
        return (center.getBlockX() + size / 2 >= location.getBlockX()) && (center.getBlockX() - size / 2 <= location.getBlockX()) &&
                (center.getBlockZ() + size / 2 >= location.getBlockZ()) && (center.getBlockZ() - size / 2 <= location.getBlockZ());
    }


    public List<Flag> getFlaresMembers() {
        return flaresMembers;
    }

    public List<Flag> getFlaresExternal() {
        return flaresExternal;
    }

    public UUID getOwner() {
        return owner;
    }

    public List<UUID> getMembers() {
        return members;
    }

    public boolean isMember(UUID uuid) {
        return members.contains(uuid) || owner.equals(uuid);
    }

    public int getId() {
        return id;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
