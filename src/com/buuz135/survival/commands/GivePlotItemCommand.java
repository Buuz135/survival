package com.buuz135.survival.commands;

import com.buuz135.survival.protectedarea.SurvivalPlot;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GivePlotItemCommand extends Command {

    private List<String> playersToGive;
    public static final String NAME = ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Parcela";


    public GivePlotItemCommand(String name, Plugin plugin) {
        super(name);
        Bukkit.getPluginManager().registerEvents(new GiveEventHandler(), plugin);
        playersToGive = new ArrayList<>();
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        if (!(commandSender instanceof Player) || commandSender.isOp()) {
            if (strings.length > 0) {
                Player player = Bukkit.getPlayer(strings[0]);
                if (player != null) {
                    if (player.isOnline()) {
                        giveBanner(player);
                    }
                } else {
                    playersToGive.add(strings[0]);
                }
            }
        }
        return false;
    }

    public static void giveBanner(Player player) {
        ItemStack stack = new ItemStack(Material.BANNER, 1);
        BannerMeta meta = (BannerMeta) stack.getItemMeta();
        meta.setBaseColor(DyeColor.PURPLE);
        meta.setDisplayName(NAME);
        meta.setLore(Arrays.asList(ChatColor.BLUE + "Dueño: " + ChatColor.DARK_AQUA + player.getName(), ChatColor.BLUE + "Tamaño: " + ChatColor.DARK_AQUA + SurvivalPlot.MIN_AREA + "x" + SurvivalPlot.MIN_AREA,
                ChatColor.GRAY + "*Colócalo en el suelo para crear tu parcela*"));
        stack.setItemMeta(meta);
        player.getInventory().addItem(stack);
    }


    private class GiveEventHandler implements Listener {


        @EventHandler
        public void onJoin(PlayerJoinEvent event) {
            if (playersToGive.contains(event.getPlayer().getName())) {
                giveBanner(event.getPlayer());
                playersToGive.remove(event.getPlayer().getName());
            }
        }
    }
}
