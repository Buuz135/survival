package com.buuz135.survival.commands;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.shop.Shop;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class CreateShopCommand extends Command {

    public CreateShopCommand(String name) {
        super(name);
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = Bukkit.getPlayer(commandSender.getName());
        if (player != null) {
            HashSet<Material> materials = new HashSet<Material>();
            materials.add(Material.AIR);
            Block looking = player.getTargetBlock(materials, 10);
            if (looking.getType().name().contains("STEP")) {
                if (looking.getData() >= 8) {
                    Sign sign = null;
                    for (BlockFace face : new BlockFace[]{BlockFace.NORTH, BlockFace.WEST, BlockFace.EAST, BlockFace.SOUTH}) {
                        if (looking.getRelative(face).getType() == Material.WALL_SIGN) {
                            boolean hasLines = false;
                            sign = (Sign) looking.getRelative(face).getState();
                            for (String l : sign.getLines()) {
                                if (!l.equals("")) {
                                    hasLines = true;
                                    break;
                                }
                            }
                            if (!hasLines) {
                                break;
                            } else {
                                sign = null;
                            }

                        }
                    }
                    if (sign != null) {
                        int buy = 0;
                        int sell = 0;
                        try {
                            buy = Integer.parseInt(strings[0]);
                            sell = Integer.parseInt(strings[1]);
                        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "No se han introducido los valores correctos. (/tienda <comprar> <vender>)");
                            return false;
                        }
                        if (buy == 0 || sell == 0 || buy < sell) {
                            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "No se han introducido los valores correctos. (/tienda <comprar> <vender>)");
                        } else {
                            if (player.getItemInHand().getType() != Material.AIR) {
                                new Shop(player, player.getItemInHand(), sell, buy, player.getGameMode() == GameMode.CREATIVE, looking.getLocation(), sign.getLocation());
                                return true;
                            } else {
                                player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "No tienes un item correcto en la mano. (/tienda <comprar> <vender>)");
                            }

                        }
                    } else {
                        player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "La tienda no tiene un cartel disponible. (/tienda <comprar> <vender>)");
                    }
                } else {
                    player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "El bloque que estas mirando no es una slab en la posicion de arriba. (/tienda <comprar> <vender>)");
                }
            } else {
                player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "El bloque que estas mirando no es una slab. (/tienda <comprar> <vender>)");
            }

        }
        return false;
    }
}
