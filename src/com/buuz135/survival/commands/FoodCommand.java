package com.buuz135.survival.commands;

import com.buuz135.survival.foods.FoodPlant;
import com.buuz135.survival.foods.FoodRecipe;
import com.buuz135.survival.foods.FoodTree;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class FoodCommand extends Command {

    public FoodCommand(String name) {
        super(name);
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Inventory inventory = Bukkit.createInventory(null, 9 * 6);
        for (FoodPlant f : FoodPlant.foodPlantList) {
            inventory.addItem(f.getItem());
        }
        for (FoodTree f : FoodTree.foodTreeList) {
            inventory.addItem(f.getItem());
        }
        for (FoodRecipe f : FoodRecipe.foodRecipeList) {
            inventory.addItem(f.getItem());
        }
        ((Player) commandSender).openInventory(inventory);
        return false;
    }
}
