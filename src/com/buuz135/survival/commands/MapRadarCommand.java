package com.buuz135.survival.commands;

import com.buuz135.survival.SurvivalMain;
import com.buuz135.survival.radar.MapRadar;
import com.buuz135.survival.radar.Waypoint;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MapRadarCommand extends Command {

    private ChatColor[] color = new ChatColor[]{ChatColor.BLACK, ChatColor.DARK_AQUA, ChatColor.DARK_GREEN, ChatColor.DARK_BLUE, ChatColor.DARK_RED, ChatColor.DARK_PURPLE, ChatColor.GOLD, ChatColor.BLUE
            , ChatColor.GREEN, ChatColor.AQUA, ChatColor.RED, ChatColor.LIGHT_PURPLE, ChatColor.YELLOW};

    public MapRadarCommand(String name) {
        super(name);
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = (Player) commandSender;
        MapRadar mapRadar = MapRadar.getRadar(player);//Todo comprovar tamaño
        if (strings.length == 0) {
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Uso: /mapa <lista/añadir/quitar/ocultar/barra/color>");
            return false;
        }
        String command = strings[0];
        if (command.equals("lista")) list(player, mapRadar);
        else if (command.equals("añadir")) add(player, mapRadar, strings);
        else if (command.equals("quitar")) remove(player, mapRadar, strings);
        else if (command.equals("ocultar")) hide(player, mapRadar, strings);
        else if (command.equals("barra")) bar(player, mapRadar, strings);
        else if (command.equals("color")) color(player, mapRadar, strings);
        else
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Uso: /mapa <lista/añadir/quitar/ocultar/barra/color>");
        return true;
    }

    private void list(Player player, MapRadar radar) {//HECHO
        int i = 1;
        player.sendMessage(SurvivalMain.TAG + ChatColor.GOLD + "Lista de puntos guardados: ");
        for (Waypoint waypoint : radar.getLocationList()) {
            TextComponent component = new TextComponent(ChatColor.GOLD + "" + " " + i + ChatColor.GRAY + " » " + waypoint.getColor() + waypoint.getName() + ChatColor.GRAY + " ");
            TextComponent coords = new TextComponent(ChatColor.GOLD + "Mundo: " + ChatColor.YELLOW + waypoint.getLocation().getWorld().getName() + ChatColor.GOLD + " X: " + ChatColor.YELLOW +
                    waypoint.getLocation().getBlockX() + ChatColor.GOLD + " Y: " + ChatColor.YELLOW + waypoint.getLocation().getBlockY() + ChatColor.GOLD + " Z: " + ChatColor.YELLOW + waypoint.getLocation().getBlockZ());
            TextComponent world = new TextComponent("[⚑]");//
            world.setColor(ChatColor.BLUE);
            world.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{coords}));
            component.addExtra(world);
            TextComponent remove = new TextComponent("[✖]");//
            remove.setColor(ChatColor.DARK_RED);
            remove.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa quitar " + i));
            remove.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GRAY + "Haz click para borrar el punto.")}));
            component.addExtra(remove);
            TextComponent hide = new TextComponent("[?]");
            if (!waypoint.isVisible()) hide = new TextComponent("[!]");
            hide.setColor(ChatColor.LIGHT_PURPLE);
            hide.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa ocultar " + i));
            hide.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GRAY + "Haz click para ocultar/mostrar el punto, '?' es visible actualmente y '!' no lo es.")}));
            component.addExtra(hide);
            TextComponent color = new TextComponent("[⚒]");
            color.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa color " + i));
            color.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GRAY + "Haz click para seleccionar el color del punto.")}));
            color.setColor(ChatColor.GREEN);
            component.addExtra(color);
            TextComponent bar = new TextComponent("[⚊]");
            bar.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa barra"));
            bar.setColor(ChatColor.YELLOW);
            bar.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(ChatColor.GRAY + "Haz click para cambiar las opciones de la barra.")}));
            color.addExtra(bar);
            player.spigot().sendMessage(component);
            ++i;
        }
    }

    private void add(Player player, MapRadar radar, String[] strings) {//HECHO
        if (strings.length >= 2) {
            String name = "";
            for (int i = 1; i < strings.length; ++i) {
                name += " " + strings[i];
            }
            name = name.substring(1);
            radar.getLocationList().add(new Waypoint(ChatColor.RED, player.getLocation(), name));
        } else {
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Uso: /mapa añadir <nombre>");
        }
    }

    private void remove(Player player, MapRadar radar, String[] strings) {//HECHO
        if (strings.length == 2) {
            int i = Integer.parseInt(strings[1]) - 1;
            if (radar.getLocationList().size() > i) {
                radar.getLocationList().remove(i);
                list(player, radar);
                player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Punto quitado satisfactoriamente.");
            }
        } else {
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Uso: /mapa quitar <id>");
        }
    }

    private void hide(Player player, MapRadar radar, String[] strings) {//HECHO
        if (strings.length == 2) {
            int i = Integer.parseInt(strings[1]) - 1;
            if (radar.getLocationList().size() > i) {
                Waypoint waypoint = radar.getLocationList().get(i);
                waypoint.setVisible(!waypoint.isVisible());
                list(player, radar);
                player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Punto ocultado/mostrado satisfactoriamente.");
            }
        } else {
            player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Uso: /mapa ocultar <id>");
        }
    }

    private static ChatColor[] barColors = new ChatColor[]{ChatColor.LIGHT_PURPLE, ChatColor.BLUE, ChatColor.RED, ChatColor.GREEN, ChatColor.YELLOW, ChatColor.DARK_PURPLE, ChatColor.WHITE};

    private static String[] segmented = new String[]{"0", "6", "10", "12", "20"};

    private void bar(Player player, MapRadar radar, String[] strings) {
        if (strings.length >= 2) {
            String command = strings[1];
            if (command.equals("ocultar")) {
                if (radar.isVisible()) {
                    radar.getBar().removePlayer(player);
                    radar.setVisible(false);
                }
            }
            if (command.equals("mostrar")) {
                if (!radar.isVisible()) {
                    radar.getBar().addPlayer(player);
                    radar.setVisible(true);
                }
            }
            if (command.equals("color")) {
                if (strings.length == 2) {
                    TextComponent textComponent = new TextComponent(SurvivalMain.TAG + ChatColor.GOLD + "Escoje un color de barra: ");
                    int i = 0;
                    for (ChatColor c : barColors) {
                        TextComponent componentColor = new TextComponent("⬛");
                        componentColor.setColor(c);
                        componentColor.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa barra color " + i));
                        TextComponent hover = new TextComponent("Haz click para seleccionar el color de la barra.");
                        hover.setColor(ChatColor.GRAY);
                        componentColor.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{hover}));
                        textComponent.addExtra(componentColor);
                        ++i;
                    }
                    player.spigot().sendMessage(textComponent);
                } else {
                    int i = Integer.parseInt(strings[2]);
                    if (BarColor.values().length > i) {
                        radar.getBar().setColor(BarColor.values()[i]);
                    }
                }
            }
            if (command.equals("segmentos")) {
                if (strings.length == 2) {
                    TextComponent textComponent = new TextComponent(SurvivalMain.TAG + ChatColor.GOLD + "Escoje los segmentos de la barra: ");
                    int i = 0;
                    for (String c : segmented) {
                        TextComponent componentColor = new TextComponent("[" + c + "]");
                        componentColor.setColor(ChatColor.YELLOW);
                        componentColor.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa barra segmentos " + i));
                        TextComponent hover = new TextComponent("Haz click para seleccionar los segmentos de la barra.");
                        hover.setColor(ChatColor.GRAY);
                        componentColor.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{hover}));
                        textComponent.addExtra(componentColor);
                        ++i;
                    }
                    player.spigot().sendMessage(textComponent);
                } else {
                    int i = Integer.parseInt(strings[2]);
                    if (BarStyle.values().length > i) {
                        radar.getBar().setStyle(BarStyle.values()[i]);
                    }
                }
            }
        } else {
            TextComponent textComponent = new TextComponent(SurvivalMain.TAG + ChatColor.GOLD + "Opciones de la barra: ");
            TextComponent show = new TextComponent("[✔]");
            show.setColor(ChatColor.BLUE);
            show.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa barra mostrar"));
            TextComponent hover = new TextComponent("Haz click para mostrar la barra.");
            hover.setColor(ChatColor.GRAY);
            show.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{hover}));
            textComponent.addExtra(show);
            TextComponent hide = new TextComponent("[✖]");
            hide.setColor(ChatColor.RED);
            hide.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa barra ocultar"));
            hover = new TextComponent("Haz click para ocultar la barra.");
            hover.setColor(ChatColor.GRAY);
            hide.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{hover}));
            textComponent.addExtra(hide);
            TextComponent color = new TextComponent("[⚒]");
            color.setColor(ChatColor.GREEN);
            color.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa barra color"));
            hover = new TextComponent("Haz click para cambiar el color de la barra.");
            hover.setColor(ChatColor.GRAY);
            color.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{hover}));
            textComponent.addExtra(color);
            TextComponent seg = new TextComponent("[⚊]");
            seg.setColor(ChatColor.YELLOW);
            seg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa barra segmentos"));
            hover = new TextComponent("Haz click para cambiar los segmentos de la barra.");
            hover.setColor(ChatColor.GRAY);
            seg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{hover}));
            textComponent.addExtra(seg);
            player.spigot().sendMessage(textComponent);
        }
    }

    private void color(Player player, MapRadar radar, String[] strings) {//HECHO
        if (strings.length == 2) {
            TextComponent textComponent = new TextComponent(SurvivalMain.TAG + ChatColor.GOLD + "Escoje un color: ");
            int i = 0;
            for (ChatColor c : color) {
                TextComponent componentColor = new TextComponent("⬛");
                componentColor.setColor(c);
                componentColor.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/mapa color " + strings[1] + " " + i));
                TextComponent hover = new TextComponent("Haz click para seleccionar el color del punto.");
                hover.setColor(ChatColor.GRAY);
                componentColor.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{hover}));
                textComponent.addExtra(componentColor);
                ++i;
            }
            player.spigot().sendMessage(textComponent);
        } else if (strings.length == 3) {
            int i = Integer.parseInt(strings[1]) - 1;
            int col = Integer.parseInt(strings[2]);
            if (radar.getLocationList().size() > i && color.length > col) {
                Waypoint waypoint = radar.getLocationList().get(i);
                waypoint.setColor(color[col]);
                list(player, radar);
                player.sendMessage(SurvivalMain.TAG + ChatColor.RED + "Color cambiado satisfactoriamente.");
            }
        }
    }
}
